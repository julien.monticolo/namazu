CREATE SCHEMA IF NOT EXISTS views;

CREATE OR REPLACE VIEW views.areas_currents AS
SELECT a.id,
       a.area_type,
       a.district,
       a.country_iso2,
       a.country_iso3,
       a.administrative_code_type,
       a.administrative_code,
       a.source,
       a.source_version,
       a.source_id,
       ap.name,
       ap.population,
       ap.latitude,
       ap.longitude,
       ap.location,
       ap.boundary,
       ap.period_start,
       ap.period_end
FROM geography.areas a
LEFT JOIN geography.areas_periods ap ON ap.area_id = a.id
WHERE ap.period_end IS NULL;

CREATE OR REPLACE VIEW views.events_all AS
SELECT e.hidden,
       e.id,
       o.id as preferred_origin_id,
       m.id as preferred_magnitude_id,
       e.event_type,
       e.merged_with_event_id,
       s.event_publicid as seiscomp_publicid,
       e.publicid as event_publicid,
       o.time,
       m.magnitude,
       m.magnitude_type,
       o.latitude,
       o.latitude_uncertainty,
       o.longitude,
       o.longitude_uncertainty,
       o.depth,
       o.depth_type,
       o.earthmodel_id,
       o.automatic,
       q.standard_error,
       q.azimuthal_gap,
       q.minimum_distance,
       q.maximum_distance,
       q.used_phase_count,
       q.associated_phase_count,
       o.location,
       o.author,
       z.name,
       z.slug
FROM seismic.events AS e
JOIN seismic.origins o ON o.event_id = e.id AND o.event_preferred
LEFT JOIN seismic.magnitudes m ON m.origin_id = o.id AND m.event_preferred
LEFT JOIN seismic.origins_qualities q ON q.origin_id = o.id
LEFT JOIN seiscomp3.mappings s ON s.event_id = e.id
LEFT JOIN namazu.zones z ON e.zone_id = z.id;

CREATE OR REPLACE VIEW views.events AS
SELECT *
FROM views.events_all
WHERE NOT hidden;

CREATE OR REPLACE VIEW views.current_countries AS
SELECT a.id,
       a.country_iso2,
       administrative_code_type,
       a.administrative_code,
       a.source,
       a.source_version,
       a.source_id,
       ap.name,
       ap.population,
       ap.latitude,
       ap.longitude,
       ap.location,
       ap.boundary,
       ap.period,
       ap.period_start,
       ap.period_end
  FROM geography.areas a
  JOIN geography.areas_periods ap ON area_id = a.id
 WHERE NOT a.district
   AND a.area_type = 'country'
   AND ap.period_end IS NULL;

CREATE OR REPLACE VIEW views.current_eez AS
SELECT a.id,
       a.country_iso2,
       administrative_code_type,
       a.administrative_code,
       a.source,
       a.source_version,
       a.source_id,
       ap.name,
       ap.latitude,
       ap.longitude,
       ap.location,
       ap.boundary,
       ap.period,
       ap.period_start,
       ap.period_end
  FROM geography.areas a
  JOIN geography.areas_periods ap ON area_id = a.id
 WHERE NOT a.district
   AND a.area_type = 'eez'
   AND ap.period_end IS NULL;

CREATE OR REPLACE VIEW views.current_cities AS
SELECT a.id,
       a.country_iso2,
       administrative_code_type,
       a.administrative_code,
       a.source,
       a.source_version,
       a.source_id,
       ap.name,
       ap.population,
       ap.latitude,
       ap.longitude,
       ap.location,
       ap.boundary,
       ap.period,
       ap.period_start,
       ap.period_end
  FROM geography.areas a
  JOIN geography.areas_periods ap ON area_id = a.id
 WHERE NOT a.district
   AND a.area_type = 'city'
   AND ap.period_end IS NULL;

CREATE OR REPLACE VIEW views.phases AS
SELECT a.origin_id,
       a.pick_id,
       s.network_code,
       s.station_code,
       s.location_code,
       s.channel_code,
       p.time,
       p.time_lower_uncertainty,
       p.time_upper_uncertainty,
       p.filter_id,
       p.method_id,
       p.phase_hint_code,
       p.phase_hint_used,
       p.polarity,
       p.automatic,
       a.phase_code,
       a.azimuth,
       a.distance,
       a.time_residual,
       a.time_weight
FROM seismic.arrivals a
JOIN seismic.picks p ON a.pick_id = p.id
JOIN seismic.streams s ON p.stream_id = s.id;

CREATE OR REPLACE VIEW views.sync_stats AS
SELECT time AS origin_time,
       created_at - time AS seiscomp_latency,
       inserted_at - created_at AS sync_latency,
       inserted_at - time AS publication_latency,
       latitude,
       longitude
FROM seismic.origins;

CREATE OR REPLACE VIEW views.stations AS
SELECT s.id,
       s.station_code,
       n.network_code,
       p.period_start,
       p.period_end,
       p.latitude,
       p.longitude,
       p.location
FROM instruments.stations AS s
JOIN instruments.networks n ON s.network_id = n.id
JOIN instruments.stations_periods p ON p.station_id = s.id;

CREATE OR REPLACE VIEW views.missing_stations AS
SELECT s.network_code,
       s.station_code,
       min(p.time) AS min_time,
       max(p.time) AS max_time,
       count(*)
FROM seismic.picks AS p,
     instruments.stations_periods AS sp,
     seismic.streams AS s
LEFT OUTER JOIN instruments.stations st ON st.station_code = s.station_code
WHERE s.id = p.stream_id
  AND st IS NULL
   OR p.time <@ sp.period
GROUP BY s.network_code, s.station_code
ORDER BY count(*) DESC;

CREATE OR REPLACE VIEW views.arrivals_vs_used_phase_count AS
SELECT m.event_publicid AS seiscomp_publicid,
       q.origin_id AS nmz_origin_id,
       o.latitude AS latitude,
       o.longitude AS longitude,
       q.used_phase_count AS used_phase_count,
       count(a) AS arrival_count,
       count(a) - q.used_phase_count AS arrivals_minus_used_phase_count
FROM seismic.origins_qualities q,
     seismic.arrivals a,
     seiscomp3.mappings m,
     seismic.origins o
WHERE o.id = q.origin_id
  AND o.event_id = m.event_id
  AND q.origin_id = a.origin_id
  AND a.time_weight > 0
GROUP BY m.event_publicid,
         q.origin_id,
         o.latitude,
         o.longitude,
         q.used_phase_count
HAVING q.used_phase_count <> count(a);
