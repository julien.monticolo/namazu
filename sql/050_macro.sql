CREATE SCHEMA IF NOT EXISTS macro;

CREATE TYPE macro.address_source AS ENUM (
  'gps',
  'witness'
);

CREATE TYPE macro.answer_type AS ENUM (
  'integer',
  'string',
  'text',
  'image',
  'level'
);

CREATE TYPE macro.form_type AS ENUM (
  'city',
  'individual'
);

CREATE TABLE IF NOT EXISTS macro.forms (
  id serial PRIMARY KEY,

  name varchar NOT NULL,
  "type" macro.form_type NOT NULL,
  "default" boolean NOT NULL DEFAULT FALSE,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

/* CREATE UNIQUE INDEX unique_default_form ON macro.forms(type, "default") WHERE "default"; */

CREATE TABLE IF NOT EXISTS macro.questions (
  id serial PRIMARY KEY,
  form_id integer NOT NULL REFERENCES macro.forms(id) ON DELETE CASCADE,

  answer_type macro.answer_type NOT NULL,
  multiple boolean NOT NULL DEFAULT FALSE,
  "order" smallint NOT NULL,
  type varchar, -- Enum date, people, objects, environment, animals, buildings
  text varchar NOT NULL,
  subtext varchar,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(form_id, "order")
);

CREATE TABLE IF NOT EXISTS macro.questions_choices (
  id serial PRIMARY KEY,
  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,

  value varchar,
  "order" smallint NOT NULL,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone

  /* UNIQUE(question_id, "order") */
);

CREATE TABLE IF NOT EXISTS macro.questions_conditions (
  id serial PRIMARY KEY,
  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,
  question_choice_id integer NOT NULL REFERENCES macro.questions_choices(id) ON DELETE CASCADE,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(question_id, question_choice_id)
);

CREATE TABLE IF NOT EXISTS macro.testimonies (
  id serial PRIMARY KEY,
  city_id integer REFERENCES geography.areas(id),
  event_id integer REFERENCES seismic.events(id),
  form_id integer REFERENCES macro.forms(id),

  key uuid NOT NULL,
  felt_time timestamp with time zone,
  naive_felt_time timestamp without time zone,
  timezone character varying(30),
  highlighted boolean NOT NULL DEFAULT FALSE,
  utc_offset smallint,
  quality double precision, -- Enum poor, very poor, great
  author varchar,
  email varchar,
  source varchar,
  address varchar,
  address_source macro.address_source NOT NULL,
  expert_comment text,
  geocoding_type varchar,
  latitude double precision,
  longitude double precision,
  location geometry(Point, 4326),

  created_at timestamp with time zone
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS macro.answers (
  id serial PRIMARY KEY,
  question_choice_id integer NOT NULL REFERENCES macro.questions_choices(id),
  testimony_id integer NOT NULL REFERENCES macro.testimonies(id) ON DELETE CASCADE,

  value varchar,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(testimony_id, question_choice_id)
);

CREATE TABLE IF NOT EXISTS macro.intensities (
  id serial PRIMARY KEY,
  event_id integer REFERENCES seismic.events(id),
  testimony_id integer REFERENCES macro.testimonies(id),
  testimony_preferred boolean DEFAULT False,
  intensity double precision NOT NULL,
  intensity_lower_uncertainty double precision,
  intensity_upper_uncertainty double precision,
  intensity_type varchar NOT NULL, -- enum 'EMS-98', 'MSK' ?
  method varchar NOT NULL, -- enum 'thumbnail', 'corrected thumbnail' ?
  evaluation_status varchar, -- enum 'final', 'preliminary' NOT NULL ?
  quality double precision,
  automatic boolean NOT NULL,
  author varchar, -- public.authors ?
  notes text,
  city_id integer REFERENCES geography.areas(id),
  city_preferred boolean DEFAULT False,
  city_administrative_code_type varchar(10),
  city_administrative_code varchar(10),
  latitude double precision,
  longitude double precision,
  location geometry(Point, 4326),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE UNIQUE INDEX intensities_unique_city_preferred_idx ON macro.intensities(event_id, city_id) WHERE city_preferred;
CREATE UNIQUE INDEX intensities_unique_testimony_preferred_idx ON macro.intensities(event_id, testimony_id) WHERE testimony_preferred AND testimony_id IS NOT NULL;

CREATE TABLE IF NOT EXISTS macro.intensities_contributions (
  id serial PRIMARY KEY,
  parent_intensity_id integer NOT NULL REFERENCES macro.intensities(id),
  intensity_id integer NOT NULL REFERENCES macro.intensities(id),
  weight double precision,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE (parent_intensity_id, intensity_id)
);
