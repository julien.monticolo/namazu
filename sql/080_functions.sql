CREATE OR REPLACE FUNCTION namazu.set_location() RETURNS trigger AS $$
  BEGIN
    IF (NEW.latitude IS NOT NULL) AND (NEW.longitude IS NOT NULL) THEN
      NEW.location := ST_SetSRID(ST_Point(NEW.longitude, NEW.latitude), 4326)::geography;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.base26_encode(IN digits bigint, IN min_width int = 0) RETURNS text AS $$
DECLARE
    chars char[] := ARRAY['a','b' ,'c','d','e','f','g','h','i','j','k','l','m','n' ,'o','p','q','r','s','t','u','v','w','x','y','z'];
    ret text := '';
    val bigint := digits;
BEGIN
    IF digits < 0 THEN
        val := -val;
    END IF;

    WHILE val > 0 OR min_width > 0 LOOP
        ret := chars[(mod(val,26))+1] || ret;
        val := div(val,26);
        min_width := min_width-1;
    END LOOP;
    IF digits < 0 THEN
        ret := '-'||ret;
    END IF;
    RETURN ret;
END;
$$ LANGUAGE plpgsql IMMUTABLE;

CREATE OR REPLACE FUNCTION namazu.get_event_publicid(eventid integer, ms_ms_offset integer DEFAULT 0) RETURNS varchar AS $$
  DECLARE
    origin seismic.origins%rowtype;
    key varchar;
    timestamp bigint;
  BEGIN
    SELECT * INTO origin
    FROM seismic.origins o
    WHERE o.event_id = eventid
      AND o.event_preferred;

    IF (origin IS NULL) THEN
      RETURN NULL;
    END IF;

    timestamp := (extract(epoch from origin.time) + ms_ms_offset)::bigint;

    key := 'fr' || date_part('year', origin.time)::varchar || right(namazu.base26_encode(timestamp), 6);

    RETURN key;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_publicid() RETURNS trigger AS $$
  DECLARE
    new_publicid varchar;
    ms_offset integer;
  BEGIN
    IF NOT NEW.event_preferred THEN
      RETURN NEW;
    END IF;

    PERFORM *
    FROM seismic.events
    WHERE id = NEW.event_id
      AND publicid IS NULL;

    IF NOT FOUND THEN
      RETURN NEW;
    END IF;

    ms_offset := 0;
    new_publicid := namazu.get_event_publicid(NEW.event_id, ms_offset);

    LOOP
      new_publicid := namazu.get_event_publicid(NEW.event_id, ms_offset);

      PERFORM * FROM seismic.events WHERE publicid = new_publicid;

      IF FOUND THEN
        ms_offset := ms_offset + 1;
      ELSE
        UPDATE seismic.events
        SET publicid = new_publicid
        WHERE id = NEW.event_id;

        RETURN NEW;
      END IF;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.set_period() RETURNS trigger AS $$
  BEGIN
    IF NEW.period_end IS NOT NULL THEN
      NEW.period := '[' || NEW.period_start || ',' || NEW.period_end || ')';
    ELSE
      NEW.period := '[' || NEW.period_start || ',)';
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.set_updated_time() RETURNS trigger AS $$
  BEGIN
    NEW.updated_at := current_timestamp;
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.zone_area_set_boundary() RETURNS trigger AS $$
  BEGIN
    IF (TG_OP = 'DELETE') THEN
      UPDATE namazu.zones SET boundary = namazu.get_zone_boundary(OLD.zone_id) WHERE id = OLD.zone_id;
    ELSE
      UPDATE namazu.zones SET boundary = namazu.get_zone_boundary(NEW.zone_id) WHERE id = NEW.zone_id;
    END IF;

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION geography.set_geoms() RETURNS trigger AS $$
  BEGIN
    IF (NEW.latitude IS NOT NULL) AND (NEW.longitude IS NOT NULL) THEN
      NEW.location := ST_SetSRID(ST_Point(NEW.longitude, NEW.latitude), 4326)::geography;
    ELSIF NEW.boundary IS NOT NULL THEN
      NEW.location := public.ST_Centroid(NEW.boundary);
      NEW.location_type := 'centroid';
    END IF;

    IF (NEW.location IS NOT NULL) THEN
      NEW.latitude := public.ST_Y(NEW.location);
      NEW.longitude := public.ST_X(NEW.location);
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION geography.get_timezone(location geometry) RETURNS varchar AS $$
  DECLARE
    timezone_id varchar;
  BEGIN
    SELECT t.timezone_id INTO timezone_id
      FROM geography.timezones t
     WHERE St_Contains(t.boundary, location)
       AND t.timezone_id NOT LIKE 'Etc/GMT%';

    IF FOUND THEN
      RETURN timezone_id;
    END IF;

    SELECT t.timezone_id INTO timezone_id
      FROM geography.timezones t
     WHERE t.timezone_id NOT LIKE 'Etc/GMT%'
    ORDER BY St_Distance(t.boundary, location)
    LIMIT 1;

    IF FOUND THEN
      RETURN timezone_id;
    END IF;

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.get_zone_boundary(zone_id integer) RETURNS geometry AS $$
  #variable_conflict use_variable
  DECLARE
    envelope geometry;
    zone namazu.zones%rowtype;
    areas_ids integer[];
  BEGIN
    SELECT * INTO zone FROM namazu.zones WHERE id = zone_id;

    envelope := ST_MakeEnvelope(zone.minimal_longitude, zone.minimal_latitude, zone.maximal_longitude, zone.maximal_latitude, 4326);

    SELECT array_agg(id) INTO areas_ids FROM geography.areas a, namazu.zones_areas z WHERE z.area_id = a.id AND z.zone_id = zone_id;

    IF areas_ids IS NOT NULL THEN
      RETURN St_Multi(
        St_Intersection(
          St_Union(
            array_agg(
              ST_SimplifyPreserveTopology(St_Buffer(boundary::geography, zone.buffer * 1000)::geometry, 0.1)
            )
          ),
          envelope
        )
      ) FROM (
        SELECT boundary FROM geography.areas_periods WHERE area_id = ANY(areas_ids)
      ) AS boundaries;
    ELSE
      RETURN St_Multi(envelope);
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.zone_set_boundary() RETURNS trigger AS $$
  BEGIN
    NEW.boundary := namazu.get_zone_boundary(NEW.id);

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.get_zone(location geometry) RETURNS namazu.zones AS $$
  DECLARE
    zone namazu.zones%rowtype;
  BEGIN
    SELECT * INTO zone
      FROM namazu.zones z
     WHERE St_Contains(z.boundary, location)
    ORDER BY z.weight
    LIMIT 1;

    IF FOUND THEN
      RETURN zone;
    END IF;

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.is_event_hidden(event_id integer) RETURNS boolean AS $$
  #variable_conflict use_variable
  DECLARE
    event seismic.events%rowtype;
  BEGIN
    SELECT * INTO event FROM seismic.events WHERE id = event_id;

    IF event.event_type IN  ('not locatable', 'not existing') THEN
      RETURN true;
    END IF;

    PERFORM * FROM seismic.events e
      JOIN seismic.origins o ON o.event_id = e.id AND o.event_preferred
      LEFT JOIN seismic.origins_qualities q ON q.origin_id = o.id
      JOIN seismic.magnitudes m ON m.origin_id = o.id AND m.event_preferred
      JOIN namazu.zones z ON z.id = e.zone_id
      WHERE e.id = event_id
        AND o.automatic
        AND o.depth <= z.automatic_maximal_depth
        AND m.magnitude >= z.automatic_minimal_magnitude
        AND q.used_phase_count >= z.automatic_minimal_used_phase_count
        AND z.show_automatic;

    IF FOUND THEN
      RETURN false;
    END IF;

    PERFORM * FROM seismic.events e
      JOIN seismic.origins o ON o.event_id = e.id AND o.event_preferred
      LEFT JOIN seismic.origins_qualities q ON q.origin_id = o.id
      JOIN seismic.magnitudes m ON m.origin_id = o.id AND m.event_preferred
      JOIN namazu.zones z ON z.id = e.zone_id
      WHERE e.id = event_id
        AND NOT o.automatic
        AND o.depth <= z.manual_maximal_depth
        AND m.magnitude >= z.manual_minimal_magnitude
        AND q.used_phase_count >= z.manual_minimal_used_phase_count
        AND z.show_manual;

    IF FOUND THEN
      RETURN false;
    END IF;

    RETURN true;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_hidden() RETURNS trigger AS $$
  BEGIN
    NEW.hidden = namazu.is_event_hidden(NEW.id);

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.manage_merged_event() RETURNS trigger AS $$
  DECLARE
    origin_count integer;
  BEGIN
    IF OLD.event_id <> NEW.event_id THEN
      SELECT count(*) INTO origin_count FROM seismic.origins WHERE id <> OLD.id AND event_id = OLD.event_id;

      UPDATE seismic.magnitudes SET event_id = NEW.event_id WHERE origin_id = NEW.id;

      IF origin_count = 0 THEN
        UPDATE seismic.events SET event_type = 'not existing', merged_with_event_id = NEW.event_id WHERE id = OLD.event_id;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_origin_preferred() RETURNS trigger AS $$
  BEGIN
    IF NEW.event_preferred THEN
      IF (TG_OP = 'INSERT') THEN
        UPDATE seismic.origins SET event_preferred = false WHERE event_id = NEW.event_id AND event_preferred;
      ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE seismic.origins SET event_preferred = false WHERE event_id = NEW.event_id AND event_preferred AND id <> OLD.id;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_timezone() RETURNS trigger AS $$
  #variable_conflict use_variable
  DECLARE
    timezone varchar;
  BEGIN
    IF NEW.event_preferred THEN
      timezone := geography.get_timezone(NEW.location);
      UPDATE seismic.events
      SET timezone = timezone, "localtime" = NEW.time AT TIME ZONE timezone
      WHERE id = NEW.event_id;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_zone() RETURNS trigger AS $$
  BEGIN
    IF NEW.event_preferred THEN
      UPDATE seismic.events SET zone_id = (namazu.get_zone(NEW.location)).id WHERE id = NEW.event_id;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_hidden() RETURNS trigger AS $$
  BEGIN
    IF NEW.event_preferred THEN
      UPDATE seismic.events SET hidden = namazu.is_event_hidden(id) WHERE id = NEW.event_id;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_distinctive_city() RETURNS trigger AS $$
  DECLARE
    city views.current_cities%rowtype;
  BEGIN
    IF NEW.event_preferred THEN
      SELECT * INTO city FROM geography.get_distinctive_city(NEW.location, 10);

      IF FOUND THEN
        UPDATE seismic.events SET
          distinctive_city_id = city.id,
          distinctive_city_distance = (ST_Distance(NEW.location::geography, city.location::geography, false) / 1000)::integer
        WHERE id = NEW.event_id;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_magnitude_preferred() RETURNS trigger AS $$
  BEGIN
    IF NEW.event_preferred THEN
      IF (TG_OP = 'INSERT') THEN
        UPDATE seismic.magnitudes SET event_preferred = false WHERE event_id = NEW.event_id AND event_preferred;
      ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE seismic.magnitudes SET event_preferred = false WHERE event_id = NEW.event_id AND event_preferred AND id <> OLD.id;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION macro.set_testimony_timezone() RETURNS trigger AS $$
  BEGIN
    NEW.timezone = geography.get_timezone(NEW.location);

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION macro.set_testimony_felt_time() RETURNS trigger AS $$
  BEGIN
    IF NEW.naive_felt_time IS NOT NULL AND NEW.timezone IS NOT NULL AND NEW.felt_time IS NULL THEN
      NEW.felt_time = NEW.naive_felt_time AT TIME ZONE NEW.timezone AT TIME ZONE 'UTC';
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION geography.get_nearest_cities(loc geography, lim integer) RETURNS SETOF views.current_cities AS $$
  DECLARE
    r views.current_cities%rowtype;
  BEGIN
    FOR r IN SELECT *
      FROM views.current_cities c
      WHERE c.population > 5000
      ORDER BY c.location <-> loc LIMIT lim
    LOOP
      RETURN NEXT r;
    END LOOP;
    RETURN;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION geography.get_distinctive_city(loc geography, lim integer) RETURNS views.current_cities AS $$
  DECLARE
    r views.current_cities%rowtype;
  BEGIN
    SELECT * FROM geography.get_nearest_cities(loc, lim) c ORDER BY c.population DESC LIMIT 1 INTO r;

    IF FOUND THEN
      RETURN r;
    END IF;

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;
