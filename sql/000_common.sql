CREATE EXTENSION postgis;
CREATE EXTENSION btree_gist;

CREATE SCHEMA IF NOT EXISTS namazu;
CREATE SCHEMA IF NOT EXISTS seismic;

CREATE TYPE seismic.depth_type AS ENUM (
  'from location',
  'from moment tensor inversion',
  'from modeling of broad-band P waveforms',
  'constrained by depth phases',
  'constrained by direct phases',
  'constrained by depth and direct phases',
  'operator assigned',
  'other'
);

CREATE TYPE seismic.evaluation_status AS ENUM (
  'confirmed',
  'final',
  'preliminary',
  'rejected',
  'reviewed'
);

CREATE TYPE seismic.event_type AS ENUM (
  'accidental explosion',
  'acoustic noise',
  'anthropogenic event',
  'atmospheric event',
  'avalanche',
  'blasting levee',
  'boat crash',
  'building collapse',
  'cavity collapse',
  'chemical explosion',
  'collapse',
  'controlled explosion',
  'crash',
  'debris avalanche',
  'earthquake',
  'experimental explosion',
  'explosion',
  'fluid extraction',
  'fluid injection',
  'hydroacoustic event',
  'ice quake',
  'induced', -- QuakeML : induced or triggered event
  'industrial explosion',
  'landslide',
  'meteorite',
  'mine collapse',
  'mining explosion',
  'not existing',
  'not locatable', -- Imported from SeisComp3
  'not reported',
  'nuclear explosion',
  'other event',
  'outside of network interest', -- Imported from SeisComp3
  'plane crash',
  'quarry blast',
  'reservoir loading',
  'road cut',
  'rock burst',
  'rockslide',
  'slide',
  'snow avalanche',
  'sonic blast',
  'sonic boom',
  'thunder',
  'train crash',
  'volcanic eruption'
);

CREATE TYPE seismic.origin_type AS ENUM (
  'amplitude',
  'centroid',
  'hypocenter',
  'macroseismic',
  'rupture end',
  'rupture start'
);
