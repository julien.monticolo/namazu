CREATE DOMAIN seismic.network_code AS character varying(2) NOT NULL;
CREATE DOMAIN seismic.station_code AS character varying(5) NOT NULL;
CREATE DOMAIN seismic.location_code AS character varying(2);
CREATE DOMAIN seismic.channel_code AS character varying(3);

CREATE TABLE IF NOT EXISTS seismic.events (
  id serial PRIMARY KEY,
  zone_id integer REFERENCES namazu.zones(id),
  merged_with_event_id integer REFERENCES seismic.events(id),

  publicid varchar(12) UNIQUE,
  event_type seismic.event_type,
  -- m_typecertainty
  "localtime" timestamp with time zone,
  timezone character varying(30),
  hidden boolean DEFAULT false,
  felt boolean DEFAULT false,

  distinctive_city_id integer REFERENCES geography.areas(id),
  distinctive_city_distance integer,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS seismic.streams (
  id serial PRIMARY KEY,

  network_code seismic.network_code,
  station_code seismic.station_code,
  location_code seismic.location_code,
  channel_code seismic.channel_code,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE UNIQUE INDEX streams_unique_location_code_not_null_idx ON seismic.streams(network_code, station_code, location_code, channel_code) WHERE location_code IS NOT NULL;
CREATE UNIQUE INDEX streams_unique_location_code_null_idx ON seismic.streams(network_code, station_code, channel_code) WHERE location_code IS NULL;

CREATE TABLE IF NOT EXISTS seismic.picks (
  id serial PRIMARY KEY,
  stream_id integer NOT NULL REFERENCES seismic.streams(id),

  "time" timestamp with time zone NOT NULL,
  -- m_time_uncertainty double precision,
  time_lower_uncertainty double precision,
  time_upper_uncertainty double precision,
  -- m_time_confidencelevel
  -- m_waveformid_resourceuri
  filter_id character varying(255),
  method_id character varying(255),
  -- m_horizontalslowness_value
  -- m_horizontalslowness_uncertainty
  -- m_horizontalslowness_loweruncertainty
  -- m_horizontalslowness_upperuncertainty
  -- m_horizontalslowness_confidencelevel
  -- m_horizontalslowness_used
  -- m_backazimuth_value
  -- m_backazimuth_uncertainty
  -- m_backazimuth_loweruncertainty
  -- m_backazimuth_upperuncertainty
  -- m_backazimuth_confidencelevel
  -- m_backazimuth_used
  -- m_slownessmethodid
  -- m_onset
  phase_hint_code character varying(4) NOT NULL,
  phase_hint_used boolean NOT NULL,
  polarity character varying(255),
  -- m_evaluationstatus
  automatic boolean,
  agency character varying(255),
  author character varying(255),

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX picks_time_idx ON seismic.picks("time");

CREATE TABLE IF NOT EXISTS seismic.origins (
  id serial PRIMARY KEY,
  event_id integer NOT NULL REFERENCES seismic.events(id),

  event_preferred boolean NOT NULL DEFAULT false,
  "time" timestamp with time zone NOT NULL,
  time_uncertainty double precision,
  -- m_time_loweruncertainty
  -- m_time_upperuncertainty
  time_confidence_level double precision,
  latitude double precision NOT NULL,
  latitude_uncertainty double precision,
  -- m_latitude_loweruncertainty
  -- m_latitude_upperuncertainty
  latitude_confidence_level double precision,
  longitude double precision NOT NULL,
  longitude_uncertainty double precision,
  -- m_longitude_loweruncertainty
  -- m_longitude_upperuncertainty
  longitude_confidence_level double precision,
  depth double precision,
  depth_uncertainty double precision,
  -- m_depth_loweruncertainty
  -- m_depth_upperuncertainty
  -- m_depth_confidencelevel
  depth_used boolean NOT NULL DEFAULT false,
  depth_type seismic.depth_type NOT NULL,
  -- m_timefixed
  -- m_epicenterfixed
  -- m_referencesystemid
  method_id character varying(255),
  earthmodel_id character varying(255),
  -- m_type
  automatic boolean,
  evaluation_status seismic.evaluation_status,
  origin_type seismic.origin_type NOT NULL,
  location geometry(Point, 4326),
  agency character varying(255),
  author character varying(255),

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX origins_coordinates_idx ON seismic.origins(longitude, latitude);
CREATE INDEX origins_time_idx ON seismic.origins("time");
CREATE INDEX origins_depth_idx ON seismic.origins(depth);
CREATE INDEX origins_automatic_idx ON seismic.origins(automatic);
CREATE INDEX origins_location_idx ON seismic.origins USING GIST(location);
CREATE UNIQUE INDEX origins_unique_event_preferred_idx ON seismic.origins(event_id) WHERE event_preferred;

CREATE TABLE IF NOT EXISTS seismic.origins_qualities (
  origin_id integer NOT NULL UNIQUE REFERENCES seismic.origins(id) ON DELETE CASCADE,

  associated_phase_count integer,
  used_phase_count smallint,
  associated_station_count smallint,
  used_station_count smallint,
  -- m_quality_depthphasecount
  standard_error double precision,
  azimuthal_gap double precision,
  -- m_quality_secondaryazimuthalgap
  -- m_quality_groundtruthlevel
  maximum_distance double precision,
  minimum_distance double precision,
  median_distance double precision,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS seismic.origins_uncertainties (
  origin_id integer NOT NULL UNIQUE REFERENCES seismic.origins(id) ON DELETE CASCADE,

  horizontal double precision,
  minimum_horizontal double precision,
  maximum_horizontal double precision,
  -- m_uncertainty_azimuthmaxhorizontaluncertainty
  confidenceellipsoid_semimajoraxislength double precision,
  confidenceellipsoid_semiminoraxislength double precision,
  confidenceellipsoid_semiintermediateaxislength double precision,
  confidenceellipsoid_majoraxisplunge double precision,
  confidenceellipsoid_majoraxisazimuth double precision,
  confidenceellipsoid_majoraxisrotation double precision,
  confidenceellipsoid_used boolean NOT NULL DEFAULT false,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS seismic.arrivals (
  id serial PRIMARY KEY,
  origin_id integer NOT NULL REFERENCES seismic.origins(id) ON DELETE CASCADE,
  pick_id integer NOT NULL REFERENCES seismic.picks(id),

  phase_code character varying(255) NOT NULL,
  -- m_timecorrection
  azimuth double precision,
  distance double precision,
  takeoff_angle double precision,
  -- m_takeoffangle
  time_residual double precision,
  -- m_horizontalslownessresidual
  -- m_backazimuthresidual
  -- m_timeused
  -- m_horizontalslownessused
  -- m_backazimuthused
  time_weight double precision,
  -- m_earthmodelid
  -- m_preliminary

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE (origin_id, pick_id)
);

CREATE TABLE IF NOT EXISTS seismic.amplitudes (
  id serial PRIMARY KEY,
  pick_id integer NOT NULL REFERENCES seismic.picks(id),
  stream_id integer NOT NULL REFERENCES seismic.streams(id),

  amplitude double precision NOT NULL,
  amplitude_type character varying(16) NOT NULL,
  -- m_amplitude_loweruncertainty
  -- m_amplitude_upperuncertainty
  -- m_amplitude_confidencelevel
  -- m_amplitude_used
  time_window_reference timestamp with time zone NOT NULL,
  time_window_begin double precision NOT NULL,
  time_window_end double precision NOT NULL,
  time_window_used boolean NOT NULL DEFAULT false,
  period_value double precision,
  -- m_period_uncertainty
  -- m_period_loweruncertainty
  -- m_period_upperuncertainty
  -- m_period_confidencelevel
  period_used boolean NOT NULL DEFAULT false,
  snr double precision,
  -- m_unit
  -- m_waveformid_resourceuri
  waveformid_used boolean NOT NULL DEFAULT false,
  filter_id character varying(255),
  -- m_methodid
  -- m_scalingtime_value
  -- m_scalingtime_uncertainty
  -- m_scalingtime_loweruncertainty
  -- m_scalingtime_upperuncertainty
  -- m_scalingtime_confidencelevel
  -- m_scalingtime_used
  -- m_magnitudehint
  automatic boolean,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX amplitudes_pick_id_idx ON seismic.amplitudes(pick_id);
CREATE INDEX amplitudes_stream_id_idx ON seismic.amplitudes(stream_id);

CREATE TABLE IF NOT EXISTS seismic.stationmagnitudes (
  id serial PRIMARY KEY,
  amplitude_id integer NOT NULL REFERENCES seismic.amplitudes(id),
  stream_id integer NOT NULL REFERENCES seismic.streams(id),

  magnitude double precision NOT NULL,
  -- m_magnitude_uncertainty
  -- m_magnitude_loweruncertainty
  -- m_magnitude_upperuncertainty
  -- m_magnitude_confidencelevel
  magnitude_type character varying(255) NOT NULL,
  -- m_methodid
  -- m_waveformid_resourceuri
  -- m_waveformid_used

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX stationmagnitudes_amplitude_id_idx ON seismic.stationmagnitudes(amplitude_id);
CREATE INDEX stationmagnitudes_stream_id_idx ON seismic.stationmagnitudes(stream_id);

CREATE TABLE IF NOT EXISTS seismic.magnitudes (
  id serial PRIMARY KEY,
  event_id integer NOT NULL REFERENCES seismic.events(id),
  origin_id integer NOT NULL REFERENCES seismic.origins(id),

  event_preferred boolean NOT NULL DEFAULT false,
  magnitude double precision NOT NULL,
  magnitude_uncertainty double precision,
  -- m_magnitude_uncertainty
  -- m_magnitude_loweruncertainty
  -- m_magnitude_upperuncertainty
  -- m_magnitude_confidencelevel
  magnitude_type character varying(255) NOT NULL,
  method_id character varying(255),
  station_count smallint,
  -- m_azimuthalgap
  evaluation_status seismic.evaluation_status,
  agency character varying(255),
  author character varying(255),

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE UNIQUE INDEX magnitudes_unique_event_preferred_idx ON seismic.magnitudes(event_id) WHERE event_preferred;

CREATE INDEX magnitudes_origin_id_idx ON seismic.magnitudes(origin_id);
CREATE INDEX magnitudes_magnitude_idx ON seismic.magnitudes(magnitude);

CREATE TABLE IF NOT EXISTS seismic.stationmagnitudes_contributions (
  id serial PRIMARY KEY,
  magnitude_id integer NOT NULL REFERENCES seismic.magnitudes(id) ON DELETE CASCADE,
  station_magnitude_id integer NOT NULL REFERENCES seismic.stationmagnitudes(id) ON DELETE CASCADE,

  residual double precision,
  weight double precision NOT NULL,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX stationmagnitudes_contributions_magnitude_id_idx ON seismic.stationmagnitudes_contributions(magnitude_id);
CREATE INDEX stationmagnitudes_contributions_station_magnitude_id_idx ON seismic.stationmagnitudes_contributions(station_magnitude_id);
