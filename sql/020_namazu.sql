CREATE TABLE IF NOT EXISTS namazu.actors (
    id serial PRIMARY KEY,
    name varchar NOT NULL UNIQUE,
    acronym varchar UNIQUE,
    slug varchar NOT NULL UNIQUE,
    country_iso2 varchar(3) NOT NULL,
    url varchar,
    description varchar
);

CREATE TABLE IF NOT EXISTS namazu.parameters (
  key varchar(50) NOT NULL PRIMARY KEY,
  value varchar NOT NULL
);

CREATE TABLE IF NOT EXISTS namazu.zones (
    id serial PRIMARY KEY,
    name varchar NOT NULL UNIQUE,
    slug varchar NOT NULL UNIQUE,
    description varchar,
    introduction varchar,
    testimony_url varchar,
    weight smallint DEFAULT 0 NOT NULL,
    buffer smallint DEFAULT 0 NOT NULL,
    show_manual boolean DEFAULT true NOT NULL,
    manual_minimal_magnitude double precision NOT NULL DEFAULT -100,
    manual_maximal_depth double precision NOT NULL DEFAULT 6371,
    manual_minimal_used_phase_count smallint DEFAULT 0,
    manual_event_types seismic.event_type[] NOT NULL DEFAULT array['earthquake']::seismic.event_type[],
    show_automatic boolean DEFAULT false NOT NULL,
    automatic_minimal_magnitude double precision NOT NULL DEFAULT -100,
    automatic_maximal_depth double precision NOT NULL DEFAULT 6371,
    automatic_minimal_used_phase_count smallint DEFAULT 0,
    minimal_latitude double precision NOT NULL,
    maximal_latitude double precision NOT NULL,
    minimal_longitude double precision NOT NULL,
    maximal_longitude double precision NOT NULL,
    boundary geometry(MultiPolygon, 4326)
);

CREATE INDEX IF NOT EXISTS zones_boundary_idx ON namazu.zones USING GIST(boundary);

CREATE TABLE IF NOT EXISTS namazu.zones_areas (
    zone_id integer REFERENCES namazu.zones(id) NOT NULL,
    area_id integer REFERENCES geography.areas(id) NOT NULL
);

CREATE SCHEMA IF NOT EXISTS web;

CREATE TABLE IF NOT EXISTS web.pages (
    id serial PRIMARY KEY,
    title varchar NOT NULL,
    locale varchar NOT NULL,
    slug varchar NOT NULL,
    content text NOT NULL,
    published boolean NOT NULL DEFAULT false,
    UNIQUE (locale, slug)
);
