FROM elixir:1.10-alpine

EXPOSE 4000

RUN adduser -S -h /opt/namazu namazu
RUN apk add --no-cache bash

ENV HOME=/opt/namazu

WORKDIR $HOME

ENV MIX_ENV=prod

# Cache Elixir deps
COPY mix.exs .
COPY mix.lock .
COPY apps/namazu/config/config.exs apps/namazu/config/config.exs
COPY apps/namazu/mix.exs apps/namazu
COPY apps/seiscomp3/config/config.exs apps/seiscomp3/config/config.exs
COPY apps/seiscomp3/mix.exs apps/seiscomp3
COPY apps/web/config/config.exs apps/web/config/config.exs
COPY apps/web/mix.exs apps/web
COPY config/config.exs config/config.exs
COPY config/prod.exs config/prod.exs

RUN mix do local.hex --force, local.rebar --force
RUN mix deps.get --only $MIX_ENV
RUN mix deps.compile

COPY . $HOME

WORKDIR $HOME/apps/web
RUN mix phx.digest
RUN ln -s /opt/namazu/_build/prod/rel/namazu/bin/namazu /usr/local/bin/namazu

RUN chown -R namazu /opt/namazu
USER namazu

WORKDIR $HOME
RUN mix release

CMD ["namazu", "start"]
