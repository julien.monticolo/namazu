#!/bin/bash

set -e

cd apps/web && sassc assets/app.scss priv/static/css/app.css && cp -Rv assets/vendor/fontello/font priv/static/font
