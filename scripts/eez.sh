#!/bin/bash

eez_dir=$1
eez_version=$(find $eez_dir -name 'eez_v*.shp' | rev | cut -d '_' -f 1 | rev | cut -c 2- | cut -d '.' -f 1)

ogr2ogr -f GeoJSON /vsistdout/ "$eez_dir/eez_v$eez_version.shp"| jq """
{
  type: .type,
  name: .name,
  crs: .crs,
  features: [.features[] |
  {
    type: \"Feature\",
    geometry: .geometry,
    properties: {
      country_iso3: .properties.ISO_SOV1,
      source: \"MarineRegions\",
      source_version: \"$eez_version\",
      source_id: (.properties.MRGID)|tostring,
      latitude: .properties.Y_1,
      longitude: .properties.X_1,
      name: .properties.GEONAME,
    }
  }]
}
"""
