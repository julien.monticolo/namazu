# Namazu

## Based on Open Source project

| Project name      | Official Website              |
|-------------------|-------------------------------|
| Bulma             | https://bulma.io/             |
| Font Awesome      | https://fontawesome.com/      |
| Elixir            | https://elixir-lang.org/      |
| Phoenix Framework | https://phoenixframework.org/ |
| PostgreSQL        | https://www.postgresql.org/   |
