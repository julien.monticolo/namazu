# Namazu

## Basé sur des projets Open Source

| Nom du projet     | Site officiel                 |
|-------------------|-------------------------------|
| Bulma             | https://bulma.io/             |
| Font Awesome      | https://fontawesome.com/      |
| Elixir            | https://elixir-lang.org/      |
| Phoenix Framework | https://phoenixframework.org/ |
| PostgreSQL        | https://www.postgresql.org/   |
