defmodule NamazuProject.Mixfile do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [plt_add_deps: :transitive, plt_add_apps: [:mix]],
      version: "0.0.1",
      releases: [
        namazu: [
          applications: [
            namazu: :permanent,
            seiscomp3: :permanent,
            web: :permanent
          ]
        ]
      ]
    ]
  end

  def deps do
    [
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc.3", only: [:dev], runtime: false}
    ]
  end
end
