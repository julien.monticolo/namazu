import Config

for config <- "../apps/*/config/config.exs" |> Path.expand(__DIR__) |> Path.wildcard() do
  import_config config
end

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:application]

config :phoenix, :json_library, Jason

config :namazu, :environment, :dev

config :geo_postgis, json_library: Jason

config :tzdata, :autoupdate, :disabled

config :namazu, Namazu.Repo, timeout: 60_000_000

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

if Mix.env() == :prod, do: config(:logger, level: :info)
