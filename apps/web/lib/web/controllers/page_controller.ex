defmodule Web.PageController do
  import Plug.Conn
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Seismic
  alias Namazu.Zone
  alias Namazu.Seismic.Event

  use Web, :controller

  def index(conn, _params) do
    redirect(conn, to: "/fr")
  end

  def dashboard(conn, _params = %{"locale" => locale, "zone_slug" => zone_slug}) do
    zone = Namazu.Zone |> Namazu.Repo.get_by(slug: zone_slug)
    events_limit = 30

    events =
      Namazu.Zone.get_events(zone)
      |> Seismic.preload_distinctive_cities()
      |> Seismic.descending_time()
      |> limit(^events_limit)
      |> Repo.all()

    render(conn, "dashboard.html",
      locale: locale,
      zone: zone,
      events: events,
      layout: {Web.LayoutView, "dashboard_app.html"}
    )
  end
end
