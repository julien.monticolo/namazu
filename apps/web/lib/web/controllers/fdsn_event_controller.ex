defmodule Web.FDSNEventController do
  import Plug.Conn

  alias Namazu.Seismic
  alias Namazu.Repo
  import Ecto.Query
  import Namazu.Helpers

  use Web, :controller

  @version 1.2

  # "catalog",
  # "contributor",

  def trim_xml(xml) do
    xml
    |> String.split("\n")
    |> Enum.map(&String.trim/1)
    |> Enum.join()
  end

  def parse_event_types(_event_types = ""), do: nil
  def parse_event_types(_event_types = nil), do: nil

  def parse_event_types(event_types) do
    String.split(event_types, ",")
    |> Enum.map(&Namazu.Seismic.Event.quakeml_to_namazu_event_type(&1))
  end

  def fdsn_event_query(params) do
    Namazu.Search.EventSearch.query(%Namazu.Search.EventSearch{
      start_time: Map.get(params, "starttime") |> parse_datetime(),
      end_time: Map.get(params, "endtime") |> parse_datetime(),
      minimal_longitude: Map.get(params, "minlongitude") |> parse_float(),
      maximal_longitude: Map.get(params, "maxlongitude") |> parse_float(),
      minimal_latitude: Map.get(params, "minlatitude") |> parse_float(),
      maximal_latitude: Map.get(params, "maxlatitude") |> parse_float(),
      minimal_magnitude: Map.get(params, "minmagnitude") |> parse_float(),
      maximal_magnitude: Map.get(params, "maxmagnitude") |> parse_float(),
      minimal_depth: Map.get(params, "mindepth") |> parse_float(),
      maximal_depth: Map.get(params, "maxdepth") |> parse_float(),
      event_types: Map.get(params, "eventtype") |> parse_event_types(),
      event_publicid: Map.get(params, "eventid"),
      limit: Map.get(params, "limit") |> parse_integer(),
      offset: Map.get(params, "offset") |> parse_integer(),
      order_by: Map.get(params, "orderby") |> parse_order_by(),
      updated_after: Map.get(params, "updatedafter") |> parse_datetime()
    })
  end

  def get_event_ids_chunks(params, chunk_size \\ 100) do
    fdsn_event_query(params)
    |> Repo.all()
    |> Enum.map(fn e -> e.id end)
    |> Enum.chunk_every(chunk_size)
  end

  def get_events_from_ids(event_ids) do
    Seismic.event_query()
    |> where([event: e], e.id in ^event_ids)
  end

  # Could be replace in Elixir 1.10 with
  # Enum.sort_by(events, & &1.preferred_origin.time, DateTime)
  def compare_datetime(d1, d2) do
    case DateTime.compare(d1, d2) do
      :lt -> true
      :eq -> true
      :gt -> false
    end
  end

  def sort_events_chunk(events, params) do
    case Map.get(params, "orderby") do
      "time" ->
        Enum.sort_by(events, & &1.preferred_origin.time, fn d1, d2 ->
          not compare_datetime(d1, d2)
        end)

      "time-asc" ->
        Enum.sort_by(events, & &1.preferred_origin.time, &compare_datetime(&1, &2))

      # events |> Enum.sort_by(fn e -> e.preferred_origin.time end, DateTime)

      "magnitude" ->
        events |> Enum.sort_by(fn e -> e.preferred_magnitude.magnitude end) |> Enum.reverse()

      "magnitude-asc" ->
        events |> Enum.sort_by(fn e -> e.preferred_magnitude.magnitude end)

      _ ->
        events
    end
  end

  def query(conn, params) do
    format = Map.get(params, "format", "xml")
    params = Map.put_new(params, "orderby", "time")
    event_uri_prefix = Namazu.get_parameter("event_uri_prefix_en")
    event_uri_prefix_fr = Namazu.get_parameter("event_uri_prefix_fr")
    event_uri_domain = Namazu.get_parameter("event_uri_domain")

    case format do
      "json" ->
        features =
          fdsn_event_query(params)
          |> Seismic.preload_distinctive_cities()
          |> Repo.all()
          |> Enum.map(fn e ->
            %{
              id: e.publicid,
              geometry: %{
                coordinates: [
                  e.preferred_origin.longitude,
                  e.preferred_origin.latitude,
                  -e.preferred_origin.depth
                ],
                type: "Point"
              },
              properties: %{
                description: %{
                  en: Web.Filters.event_description(e),
                  fr:
                    Gettext.with_locale(Web.Gettext, "fr", fn ->
                      Web.Filters.event_description(e)
                    end)
                },
                automatic: e.preferred_origin.automatic,
                mag: e.preferred_magnitude.magnitude,
                magType: e.preferred_magnitude.magnitude_type,
                # place: "TODO!!!",
                time: e.preferred_origin.time,
                type: e.event_type,
                url: %{
                  en: "https://#{event_uri_domain}#{event_uri_prefix}/#{e.publicid}",
                  fr: "https://#{event_uri_domain}#{event_uri_prefix_fr}/#{e.publicid}"
                }
              },
              type: "Feature"
            }
          end)

        json(conn, %{
          type: "FeatureCollection",
          features: features
        })

      "text" ->
        conn =
          conn
          |> put_resp_content_type("text/plain")
          |> send_chunked(:ok)

        chunk(
          conn,
          "# EventID | Time | Latitude | Longitude | Depth/km | Author | Catalog | Contributor | ContributorID | MagnitudeType | Magnitude | MagnitudeAuthor | EventLocationName | EventType\n"
        )

        for chunk <- get_event_ids_chunks(params) do
          events =
            get_events_from_ids(chunk)
            |> Seismic.preload_preferred()
            |> Seismic.preload_distinctive_cities()
            |> Seismic.preload_zone()
            |> Repo.all()
            |> sort_events_chunk(params)

          text =
            Phoenix.View.render(Web.FDSNEventView, "query.txt", events: events)
            |> String.trim_leading()

          chunk(conn, text)
        end

        conn

      _ ->
        include_all_origins = Map.get(params, "includeallorigins", "false") == "true"
        include_all_magnitudes = Map.get(params, "includeallmagnitudes", "false") == "true"
        include_arrivals = Map.get(params, "includearrivals", "false") == "true"

        domain = Web.Cache.get_parameter("domain")

        conn =
          conn
          |> put_layout(:none)
          |> put_resp_content_type("application/xml")
          |> send_chunked(:ok)

        escaped_url = Phoenix.Controller.current_url(conn, params) |> String.replace("&", "&amp;")

        chunk(
          conn,
          Phoenix.View.render(Web.FDSNEventView, "query_start.xml", url: escaped_url)
          |> trim_xml()
        )

        for chunk <- get_event_ids_chunks(params, 10) do
          events_query =
            get_events_from_ids(chunk)
            |> Seismic.preload_preferred()
            |> Seismic.preload_distinctive_cities()
            |> Seismic.preload_origins(!(include_all_origins || include_all_magnitudes))
            |> Seismic.preload_magnitudes(!(include_all_origins || include_all_magnitudes))
            |> Seismic.preload_origins_qualities()

          events =
            if include_arrivals do
              Seismic.preload_origins_arrivals(events_query)
            else
              events_query
            end
            |> Repo.all()
            |> sort_events_chunk(params)

          xml =
            Phoenix.View.render(Web.FDSNEventView, "query.xml",
              events: events,
              domain: domain,
              include_all_origins: include_all_origins,
              include_all_magnitudes: include_all_magnitudes,
              include_arrivals: include_arrivals
            )
            |> trim_xml()

          chunk(conn, xml)
        end

        chunk(conn, Phoenix.View.render(Web.FDSNEventView, "query_end.xml", %{}) |> trim_xml())

        conn
    end
  end

  def base(conn, _params) do
    text(conn, "base")
  end

  def wadl(conn, _params) do
    conn
    |> put_layout(:none)
    |> put_resp_content_type("application/xml")
    |> render("application.wadl", version: @version)
  end

  def catalogs(conn, _params) do
    conn
    |> put_layout(:none)
    |> put_resp_content_type("application/xml")
    |> render("catalog.xml")
  end

  def contributors(conn, _params) do
    conn
    |> put_layout(:none)
    |> put_resp_content_type("application/xml")
    |> render("contributors.xml")
  end

  def version(conn, _params) do
    text(conn, @version)
  end
end
