defmodule Web.Router do
  use Web, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(Web.Plugs.SetLocale, "en")
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", Web do
    pipe_through(:browser)
  end

  scope "/api/v1", Web.API.V1 do
    pipe_through(:api)

    get("/events", EventController, :index)
    get("/events/types", EventController, :types)
    get("/events/:event_publicid/cities", EventController, :cities)
    get("/events/:event_publicid/past-seismicity", EventController, :past_seismicity)
    get("/events/:event_publicid/phases", EventController, :phases)

    get("/zones", ZoneController, :index)
    get("/zones/:zone_slug", ZoneController, :show)

    get("/zones/:zone_slug/stations", ZoneController, :stations)

    get("/stations", StationController, :index)
    # get("/stations/:station_code", StationController, :show)
  end

  scope "/fdsnws/event/1", Web do
    pipe_through(:browser)

    get("/", FDSNEventController, :base)
    get("/query", FDSNEventController, :query)
    get("/application.wadl", FDSNEventController, :wadl)
    get("/catalogs", FDSNEventController, :catalogs)
    get("/contributors", FDSNEventController, :contributors)
    get("/version", FDSNEventController, :version)
  end

  scope "/:locale", Web do
    pipe_through(:browser)

    get("/search", EventController, :search)
    get("/search/results", EventController, :search_results)

    # get("/", PageController, :index, as: :index_with_locale)

    # scope "/actors" do
    #   get("/", PageController, :actors)
    # end

    get("/dashboards/:zone_slug", PageController, :dashboard)

    # get("/pages/:page_slug", PageController, :page)

    # get("/about", PageController, :about)
    # get("/webservices", PageController, :webservices)
    # get("/contact", PageController, :contact)
    # get("/:zone_slug", PageController, :zone)
    # get("/:zone_slug/stations", PageController, :zone_stations)

    # scope "/events" do
    #   get("/:event_id", EventController, :show)
    # end

    # scope "/testimony", as: :testimony do
    #   get("/new", TestimonyController, :new)
    #   get("/new/address", TestimonyController, :witness_address)
    #   get("/:testimony_key", QuestionController, :question)

    #   post("/new/address", TestimonyController, :witness_address)
    #   post("/:testimony_key", QuestionController, :form_proceed)
    #   post("/:testimony_key/next_question", QuestionController, :next_question)
    # end
  end
end
