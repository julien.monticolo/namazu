defmodule Web.API.V1.EventController do
  use Web, :controller

  alias Namazu.Seismic.Event
  alias Namazu.Geography.Area
  alias Namazu.Geography.City
  alias Namazu.Instruments.Station
  import Namazu.Helpers

  def get_geometry(event = %Event{}) do
    event.preferred_origin.location
  end

  def get_geometry(area = %Area{}) do
    area.location
  end

  def get_geometry(station = %Station{}) do
    List.first(station.periods).location
  end

  def get_properties(event = %Event{}) do
    %{
      eventID: event.id,
      time: event.preferred_origin.time,
      magnitude: event.preferred_magnitude.magnitude,
      eventType: event.event_type,
      automatic: event.preferred_origin.automatic
    }
  end

  def get_properties(area = %City{}) do
    %{
      areaID: area.id,
      population: area.population,
      countryISO2: area.country_iso2,
      name: area.name,
      distance: area.distance
    }
  end

  def get_properties(area = %Area{}) do
    %{
      areaID: area.id,
      population: area.population,
      countryISO2: area.country_iso2,
      name: area.name,
      distance: area.distance
    }
  end

  def get_properties(station = %Station{}) do
    %{
      networkcode: station.network.network_code,
      networkcolor: station.network.color,
      stationcode: station.station_code
    }
  end

  def to_feature(zone) do
    Web.Helpers.to_feature(zone, &__MODULE__.get_properties/1, &__MODULE__.get_geometry/1)
  end

  def cities(conn, _params = %{"event_publicid" => event_publicid}) do
    cities =
      Web.Cache.get_distintive_cities(event_publicid)
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, cities)
  end

  def past_seismicity(conn, _params = %{"event_publicid" => event_publicid}) do
    events =
      Web.Cache.get_past_seismicity(event_publicid)
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, events)
  end

  def format_phase(phase) do
    %{
      arrivalID: phase.id,
      originID: phase.origin_id,
      pickID: phase.pick.id,
      networkCode: phase.pick.stream.network_code,
      stationCode: phase.pick.stream.station_code,
      locationCode: phase.pick.stream.location_code,
      channelCode: phase.pick.stream.channel_code,
      azimuth: phase.azimuth,
      distance: phase.distance,
      phaseCode: phase.phase_code,
      time: phase.pick.time,
      timeResidual: phase.time_residual,
      automatic: phase.pick.automatic
    }
  end

  def phases(conn, _params = %{"event_publicid" => event_publicid}) do
    phases =
      Web.Cache.get_preferred_origin_phases(event_publicid)
      |> Enum.map(&Web.API.V1.EventController.format_phase(&1))

    json(conn, phases)
  end

  def types(conn, _params) do
    types =
      Web.Cache.get_event_types()
      |> Enum.map(fn type ->
        %{name: type.event_type, count: type.count}
      end)

    json(conn, types)
  end

  def index(conn, _params = %{"event_publicid" => event_publicid}) do
    event =
      [Web.Cache.get_event(event_publicid)]
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, [event])
  end

  def index(conn, params) do
    search = %Namazu.Search.EventSearch{
      start_time: params["start_time"],
      end_time: params["end_time"],
      minimal_longitude: parse_float(params["minimal_longitude"]),
      maximal_longitude: parse_float(params["maximal_longitude"]),
      minimal_latitude: parse_float(params["minimal_latitude"]),
      maximal_latitude: parse_float(params["maximal_latitude"]),
      minimal_magnitude: parse_float(params["minimal_magnitude"]),
      maximal_magnitude: parse_float(params["maximal_magnitude"]),
      minimal_depth: parse_float(params["minimal_depth"]),
      maximal_depth: parse_float(params["maximal_depth"]),
      event_types: String.split(params["event_types"], ","),
      limit: parse_integer(params["limit"]),
      offset: parse_integer(params["offset"]),
      order_by: parse_order_by(params["order_by"])
    }

    events =
      Namazu.Search.EventSearch.query(search)
      |> Namazu.Repo.all()
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, events)
  end

  # def show(conn, _params = %{"event_id" => event_id}) do
  #   json(conn, event)
  # end
end
