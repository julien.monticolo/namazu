defmodule Web.API.V1.ZoneController do
  use Web, :controller

  alias Namazu.Repo
  alias Namazu.Zone

  import Ecto.Query

  def get_geometry(zone) do
    zone.boundary
  end

  def get_properties(zone) do
    %{
      buffer: zone.buffer
    }
  end

  def to_feature(zone) do
    Web.Helpers.to_feature(zone, &__MODULE__.get_properties/1, &__MODULE__.get_geometry/1)
  end

  def index(conn, _params) do
    zones =
      Zone
      |> Repo.all()
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, zones)
  end

  def show(conn, _params = %{"zone_slug" => zone_slug}) do
    zone =
      Zone
      |> where([z], z.slug == ^zone_slug)
      |> Repo.all()
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, zone)
  end

  def stations(conn, _params = %{"zone_slug" => zone_slug}) do
    zone =
      Zone
      |> Repo.get_by!(slug: zone_slug)

    stations =
      Web.Cache.get_stations_by_zone(zone)
      |> Enum.map(&Web.API.V1.StationController.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, stations)
  end
end
