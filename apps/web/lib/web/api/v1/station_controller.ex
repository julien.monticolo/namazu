defmodule Web.API.V1.StationController do
  use Web, :controller

  alias Namazu.Repo

  # alias Namazu.Instruments.Station

  # def index(conn, _params) do
  #   stations =
  #     Station
  #     |> Repo.all()
  #     |> Enum.map(&__MODULE__.to_feature(&1))
  #     |> Web.Helpers.geojson()

  #   json(conn, stations)
  # end

  def index(conn, _params = %{"zone_slug" => zone_slug}) do
    zone = Repo.get_by!(Namazu.Zone, slug: zone_slug)

    stations =
      zone
      |> Web.Cache.get_stations_by_zone()
      |> Enum.map(&Web.API.V1.EventController.to_feature(&1))
      |> Web.Helpers.geojson()

    json(conn, stations)
  end
end
