defmodule Web.Helpers do
  def geojson(features) do
    %{
      type: "FeatureCollection",
      features: features
    }
  end

  def filter_fields(map, fields) do
    map
    |> Enum.filter(fn {key, _value} -> key in fields end)
    |> Enum.into(%{})
  end

  def camel_case_fields(map) do
    Enum.map(map, fn {key, value} ->
      [first | remaining] = key |> Atom.to_string() |> String.split("_")

      new_key =
        (first <> (remaining |> Enum.map(&String.capitalize(&1)) |> Enum.join()))
        |> String.to_atom()

      {new_key, value}
    end)
    |> Enum.into(%{})
  end

  def to_feature(map, get_properties_fun, get_geometry_fun) do
    geometry = get_geometry_fun.(map)

    properties = get_properties_fun.(map)

    %{
      geometry: Geo.JSON.encode!(geometry),
      properties: properties
    }
  end
end
