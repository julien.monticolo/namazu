defmodule Web.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(Web.Endpoint, []),
      Supervisor.child_spec(
        {ConCache, [name: :parameters, ttl_check_interval: false]},
        id: :concache_parameters
      ),
      Supervisor.child_spec(
        {ConCache, [name: :events, ttl_check_interval: 5000, global_ttl: 30_000]},
        id: :concache_events
      ),
      Supervisor.child_spec(
        {ConCache, [name: :misc, ttl_check_interval: 5000, global_ttl: 60_000]},
        id: :concache_misc
      ),
      Supervisor.child_spec(
        {ConCache, [name: :stations, ttl_check_interval: 60_000, global_ttl: 60_000]},
        id: :concache_stations
      ),
      Supervisor.child_spec(
        {ConCache, [name: :past_seismicity, ttl_check_interval: 60_000, global_ttl: 60_000]},
        id: :concache_past_seismicity
      ),
      Supervisor.child_spec(
        {ConCache, [name: :phases, ttl_check_interval: 60_000, global_ttl: 60_000]},
        id: :concache_phases
      ),
      Supervisor.child_spec(
        {ConCache, [name: :distintive_cities, ttl_check_interval: 5000, global_ttl: 60_000]},
        id: :concache_distintive_cities
      ),
      Supervisor.child_spec(
        {ConCache, [name: :pages, ttl_check_interval: false]},
        id: :concache_pages
      )
    ]

    opts = [strategy: :one_for_one, name: Web.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
