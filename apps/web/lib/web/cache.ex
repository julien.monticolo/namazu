defmodule Web.Cache do
  alias Namazu.Seismic
  alias Namazu.Zone
  alias Namazu.Repo
  alias Namazu.Geography
  alias Namazu.Seismic.Origin

  import Ecto.Query

  def get_actors() do
    ConCache.get_or_store(
      :misc,
      :actors,
      fn ->
        Namazu.Actor
        |> order_by([a], asc: a.country_iso2, asc: a.name)
        |> Repo.all()
      end
    )
  end

  def get_event(event_publicid) do
    ConCache.get_or_store(
      :events,
      event_publicid,
      fn ->
        Seismic.event_query()
        |> Seismic.preload_preferred()
        |> Seismic.preload_preferred_origin_quality()
        |> Seismic.preload_distinctive_cities()
        |> Seismic.preload_zone()
        |> Repo.get_by(publicid: event_publicid)
      end
    )
  end

  def get_distintive_cities(event_publicid) do
    event = get_event(event_publicid)

    ConCache.get_or_store(
      :distintive_cities,
      event_publicid,
      fn ->
        Geography.distintive_cities(event.preferred_origin.location, 15)
      end
    )
  end

  def get_preferred_origin_phases(event_publicid) do
    event = get_event(event_publicid)

    ConCache.get_or_store(
      :phases,
      event_publicid,
      fn ->
        Origin.get_used_arrivals(event.preferred_origin.id)
      end
    )
  end

  def get_event_types() do
    ConCache.get_or_store(
      :misc,
      :event_types,
      fn ->
        Repo.all(
          from(e in Namazu.Seismic.Event,
            join: po in assoc(e, :preferred_origin),
            select: %{event_type: e.event_type, count: count(e.id)},
            group_by: e.event_type,
            where: not is_nil(e.event_type),
            order_by: [desc: fragment("count")]
          )
        )
      end
    )
  end

  def get_page(locale, page_slug) do
    key = "#{locale}:#{page_slug}"
    ConCache.get_or_store(:pages, key, fn -> Web.Page.get_page(locale, page_slug) end)
  end

  def get_parameter(key) do
    ConCache.get_or_store(:parameters, key, fn -> Namazu.get_parameter(key) end)
  end

  def get_contact_phone() do
    get_parameter("contact_phone")
  end

  def get_contact_email() do
    get_parameter("contact_email")
  end

  def get_contact_address() do
    get_parameter("contact_address")
  end

  def get_contact_address_latitude() do
    get_parameter("contact_address_latitude") |> Namazu.Helpers.parse_float()
  end

  def get_contact_address_longitude() do
    get_parameter("contact_address_longitude") |> Namazu.Helpers.parse_float()
  end

  def get_site_description() do
    get_parameter("site_description")
  end

  def get_past_seismicity(event_publicid) do
    event = get_event(event_publicid)

    ConCache.get_or_store(
      :past_seismicity,
      event_publicid,
      fn ->
        Seismic.event_query()
        |> Seismic.preload_preferred()
        |> Seismic.preload_preferred_origin_quality()
        |> Seismic.preload_distinctive_cities()
        |> where(
          [e, po, pm, poq],
          ^dynamic(
            ^Seismic.minimal_longitude(event.preferred_origin.longitude - 2) and
              ^Seismic.maximal_longitude(event.preferred_origin.longitude + 2) and
              ^Seismic.minimal_latitude(event.preferred_origin.latitude - 2) and
              ^Seismic.maximal_latitude(event.preferred_origin.latitude + 2) and
              ^Seismic.only_manual() and
              ^Seismic.only_earthquakes()
          )
        )
        |> order_by([e, po, pm, poq], desc: pm.magnitude)
        |> limit(50)
        |> Repo.all()
      end
    )
  end

  def get_stations_by_zone(zone) do
    ConCache.get_or_store(
      :stations,
      zone.slug,
      fn ->
        zone
        |> Zone.get_stations()
        |> Repo.all()
      end
    )
  end
end
