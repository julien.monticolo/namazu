defmodule Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :web

  socket("/socket", Web.UserSocket, websocket: true)

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug(
    Plug.Static,
    at: "/",
    from: :web,
    gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)
  )

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket("/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket)
    plug(Phoenix.LiveReloader)
    plug(Phoenix.CodeReloader)
  end

  plug(Plug.Logger)

  plug(
    Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason
  )

  plug(Plug.MethodOverride)
  plug(Plug.Head)

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug(
    Plug.Session,
    store: :cookie,
    key: "_web_key",
    signing_salt: "4xGM30il"
  )

  plug(Corsica, origins: "*")

  plug(Web.Router)

  @doc """
  Callback invoked for dynamically configuring the endpoint.

  It receives the endpoint configuration and checks if
  configuration should be loaded from the system environment.
  """
  def init(_key, config) do
    random_secret_key = :crypto.strong_rand_bytes(32) |> Base.encode64() |> binary_part(0, 32)

    config =
      config
      |> Keyword.put(:http, [:inet6, port: System.get_env("NMZ_PORT", "4000")])
      |> Keyword.put(:url,
        host: System.get_env("NMZ_HOST", "localhost"),
        port: System.get_env("NMZ_PUBLIC_PORT", "443"),
        scheme: System.get_env("NMZ_SCHEME", "http"),
        secret_key_base: System.get_env("NMZ_SECRET", random_secret_key)
      )
      |> Keyword.put(:matomo_host, System.get_env("NMZ_MATOMO_HOST"))

    {:ok, config}
  end
end
