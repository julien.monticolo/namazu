import Config

config :web, namespace: Web

config :web, Web.Gettext, locales: ~w(en fr), default_locale: "en"

config :web, :generators, context_app: :web

config :web, Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "erxvLI5TEvDupv85zVLZyIqIvVQknXRhB34j+gB2LVlVpPeMHrn6QRxWlgqnRoN2",
  render_errors: [view: Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Web.PubSub, adapter: Phoenix.PubSub.PG2]
