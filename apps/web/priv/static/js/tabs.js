'use strict'

var Tabs = function (element) {
  var tabsSelector = '#'.concat(element, ' > .tabs li')
  var tabsContentSelector = '#'.concat(element, ' .tab-content')

  var tabs = document.querySelectorAll(tabsSelector)
  var tabsContent = document.querySelectorAll(tabsContentSelector)

  var getIndex = function (el) {
    return [...el.parentElement.children].indexOf(el)
  }

  var deactivateAllTabs = function() {
    tabs.forEach(function(tab) {
      tab.classList.remove('is-active')
    })
  }

  var hideTabsContent = function() {
    tabsContent.forEach(function(tabContent) {
      tabContent.classList.remove('is-active')
    })
  }

  var activateTabsContent = function(tab) {
    tabsContent[getIndex(tab)].classList.add('is-active')
  }

  tabs.forEach(function (tab) {
    tab.addEventListener('click', function () {
      deactivateAllTabs()
      hideTabsContent()
      tab.classList.add('is-active')
      activateTabsContent(tab)
    })
  })

  tabs[0].click()
}
