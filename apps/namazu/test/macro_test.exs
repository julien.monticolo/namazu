defmodule NamazuMacroTest do
  use ExUnit.Case

  alias Namazu.Macro.{Address, Answer, Form, Question, QuestionChoice, Testimony}
  alias Namazu.Repo
  alias Mix.Tasks.SeedIndividualForm
  alias Ecto.Migration
  import Ecto.Query

  doctest Namazu

  setup_all do
    # Seed the database
    SeedIndividualForm.run(nil)

    form = Form |> Repo.one()

    {:ok, address} =
      Address.changeset(%Address{}, %{address_source: "witness", street: "5 rue René Descartes"})
      |> Repo.insert()

    # Create fake Testimony
    {:ok, testimony_inserted} =
      Testimony.changeset(%Testimony{}, form, address, nil, %{
        key: Ecto.UUID.bingenerate(),
        felt_time: DateTime.utc_now()
      })
      |> Repo.insert()

    testimony = testimony_inserted |> Repo.preload(form: [:questions])
    form = testimony.form

    {:ok, [testimony: testimony, form: form]}
  end

  # Begin tests
  test "check database is seeded" do
    assert Form |> Repo.one() != nil
  end

  test "next question with condition", state do
    # The user responds to a question
    question_choice = QuestionChoice |> Repo.get(1)

    {:ok, answer} =
      Answer.changeset(%Answer{}, state[:testimony], question_choice, %{
        value: question_choice.value
      })
      |> Repo.insert()

    question = Form.root_question(state[:form])
    next_question = Question |> Repo.get(2)

    assert Question.get_next_question(state[:form].questions, question, state[:testimony]) ==
             {:ok, next_question}
  end

  test "next question without condition", state do
    # The user responds to a question
    question_choice = QuestionChoice |> Repo.get(2)

    {:ok, answer} =
      Answer.changeset(%Answer{}, state[:testimony], question_choice, %{
        value: question_choice.value
      })
      |> Repo.insert()

    question = Form.root_question(state[:form])

    assert Question.get_next_question(state[:form].questions, question, state[:testimony]) == {:error, "No next question"}
  end

  test "next question end of questionnaire", state do
    # The user responds to a question

    question = Question |> Repo.get(3)

    assert Question.get_next_question(state[:form].questions, question, state[:testimony]) ==
             {:error, "No next question"}
  end

  test "previous question start of questionnaire", state do
    questions = state[:form].questions
    testimony = state[:testimony]

    question =
      Question
      |> first
      |> Repo.one
      |> (&Question.get_previous_question(questions, &1, testimony)).()

    assert question == {:error, "No previous question"}
  end

end
