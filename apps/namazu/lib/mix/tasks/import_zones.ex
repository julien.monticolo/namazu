defmodule Mix.Tasks.Namazu.ImportZones do
  use Mix.Task

  alias Namazu.Repo
  alias Namazu.Zone

  @shortdoc "Import a page from a Markdown file"

  def run(args) do
    Mix.Task.run("app.start")

    {_options, file, _} = OptionParser.parse(args, strict: [])

    File.read!(file)
    |> Jason.decode!(keys: :atoms)
    |> Enum.each(&Repo.upsert(&1, Zone))
  end
end
