defmodule Mix.Tasks.Namazu.ImportMacro do
  use Mix.Task
  use Timex

  import Ecto.Query
  import Geo.PostGIS

  alias Namazu.Geography
  alias Namazu.Macro.Answer
  alias Namazu.Macro.Form
  alias Namazu.Macro.Intensity
  alias Namazu.Macro.IntensityContribution
  alias Namazu.Macro.Question
  alias Namazu.Macro.QuestionChoice
  alias Namazu.Macro.Testimony
  alias Namazu.Repo
  alias Namazu.Seismic

  @shortdoc "Import macro data from a JSON file"

  def parse_file(file) do
    with {:ok, binary} = File.read(file),
         {:ok, data} = Jason.decode(binary),
         do: data
  end

  def cast_event(event) do
    {:ok, datetime, _} = DateTime.from_iso8601(event["time"])

    %{
      id: event["id"],
      longitude: event["longitude"],
      latitude: event["latitude"],
      magnitude: event["Ml"],
      time: datetime
    }
  end

  def schema_to_kw(event) do
    %{
      longitude: event.preferred_origin.longitude,
      latitude: event.preferred_origin.latitude,
      time: event.preferred_origin.time,
      magnitude: event.preferred_magnitude.magnitude
    }
  end

  def match_event(
        %{
          longitude: longitude,
          latitude: latitude,
          magnitude: magnitude,
          time: datetime
        },
        max_distance \\ 150
      ) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> where(
      [preferred_origin: po, preferred_magnitude: pm],
      ^dynamic(
        ^Seismic.start_time(Timex.shift(datetime, seconds: -80)) and
          ^Seismic.end_time(Timex.shift(datetime, seconds: 80)) and
          ^Seismic.maximal_distance(longitude, latitude, max_distance) and
          ^Seismic.minimal_magnitude(magnitude - 0.9) and
          ^Seismic.maximal_magnitude(magnitude + 0.9)
      )
    )
    |> Repo.all()
  end

  def get_events_mapping(events) do
    events
    |> Enum.filter(fn event -> not is_nil(event["Ml"]) end)
    |> Enum.map(fn event ->
      event = cast_event(event)

      case match_event(event) do
        [] ->
          # IO.inspect(event)
          # raise("Missing event")

          nil

        matches when length(matches) == 1 ->
          {event.id, List.first(matches)}

        matches ->
          sort_by_time = fn match ->
            abs(DateTime.diff(event.time, match.preferred_origin.time))
          end

          match =
            matches
            |> Enum.sort_by(&sort_by_time.(&1))
            |> List.first()

          {event.id, match}
      end
    end)
    |> Enum.reject(&is_nil(&1))
    |> Map.new()
  end

  def import_forms(forms) do
    forms
    |> Enum.map(fn f ->
      {:ok, form} =
        %Form{
          name: f["name"],
          type: f["type"],
          default: f["default"]
        }
        |> Map.from_struct()
        |> Repo.upsert(Form)

      choices =
        f["questions"]
        |> Enum.map(fn q ->
          {:ok, question} =
            %Question{
              form_id: form.id,
              type: q["type"],
              multiple: q["multiple"],
              order: q["order"],
              text: q["text"],
              subtext: q["subtext"]
            }
            |> Map.from_struct()
            |> Repo.upsert(Question)

          q["question_choices"]
          |> Enum.map(fn c ->
            {:ok, choice} =
              %QuestionChoice{
                question_id: question.id,
                order: c["order"],
                value: c["value"]
              }
              |> Map.from_struct()
              |> Repo.upsert(QuestionChoice)

            {c["id"], choice.id}
          end)
        end)
        |> List.flatten()
        |> Map.new()

      {f["id"], %{id: form.id, choices: choices}}
    end)
    |> Map.new()
  end

  def import_testimonies(testimonies, events_mapping, forms_mapping) do
    testimonies
    |> Enum.filter(fn t ->
      if t["naive_felt_time"] do
        {:ok, datetime, _offset} = DateTime.from_iso8601(t["naive_felt_time"])
        datetime.year > 1979
      else
        true
      end
    end)
    |> Enum.map(fn t ->
      naive_felt_time =
        case t["naive_felt_time"] do
          nil ->
            nil

          _datetime ->
            {:ok, datetime, offset} = DateTime.from_iso8601(t["naive_felt_time"])
            Timex.shift(datetime, seconds: offset)
        end

      old_form_id = t["form_id"]

      new_form_id =
        if old_form_id do
          forms_mapping[old_form_id].id
        else
          nil
        end

      quality =
        case t["quality"] do
          "fiable" -> 1
          "moyennement fiable" -> 0.5
          "peu fiable" -> 0.1
          nil -> nil
        end

      longitude = t["address"]["longitude"]
      latitude = t["address"]["latitude"]

      code_insee = t["extras"]["code_insee"]

      # TEMP
      if is_nil(t["event_id"]) or Map.has_key?(events_mapping, t["event_id"]) do
        event = Map.get(events_mapping, t["event_id"])

        event_id = if event, do: event.id

        time =
          if not is_nil(event) and not is_nil(Map.get(event, :preferred_origin)) do
            event.preferred_origin.time
          else
            nil
          end

        city_id =
          (code_insee && get_city_id(code_insee, time)) || get_city_id(longitude, latitude, time)

        IO.inspect(t)
        IO.inspect(t["event_id"])

        {:ok, testimony} =
          %Testimony{
            event_id: event_id,
            form_id: new_form_id,
            city_id: city_id,
            key: t["key"],
            naive_felt_time: naive_felt_time,
            quality: quality,
            highlighted: false,
            address: t["address"]["street"],
            address_source: t["address"]["address_source"],
            geocoding_type: t["address"]["geocoding_type"],
            longitude: longitude,
            latitude: latitude,
            source: Map.get(t, "source")
          }
          |> Map.from_struct()
          |> Repo.upsert(Testimony)

        Map.get(t, "answers", [])
        |> Enum.each(fn a ->
          value = a["value"]

          {:ok, _} =
            %Answer{
              question_choice_id: forms_mapping[old_form_id][:choices][a["question_choice_id"]],
              testimony_id: testimony.id,
              value:
                cond do
                  is_integer(value) -> Integer.to_string(value)
                  true -> value
                end
            }
            |> Map.from_struct()
            |> Repo.upsert(Answer)
        end)

        {t["id"], testimony.id}
      end
    end)
    |> Enum.filter(fn t -> not is_nil(t) end)
    |> Map.new()
  end

  def convert_quality(quality) do
    case quality do
      nil -> nil
      "sûr" -> 1
      "moyennement sûr" -> 0.5
      "peu sûr" -> 0.1
    end
  end

  def get_city_id(longitude, latitude, time) do
    IO.inspect("get_city_id::coord")
    point = %Geo.Point{coordinates: {longitude, latitude}, srid: 4326}

    from(
      a in Geography.Area,
      join: ap in Geography.AreaPeriod,
      on: ap.area_id == a.id and fragment("? @> ?::timestamptz", ap.period, ^time),
      where: st_within(^point, ap.boundary) and a.area_type == "city" and not a.district,
      select: a.id
    )
    |> Repo.all()
    |> List.first()
  end

  def get_city_id(insee_code, time) do
    IO.inspect("get_city_id::insee")

    case insee_code do
      nil ->
        nil

      insee_code ->
        from(
          a in Geography.Area,
          join: ap in Geography.AreaPeriod,
          on: ap.area_id == a.id and fragment("? @> ?::timestamptz", ap.period, ^time),
          where:
            a.area_type == "city" and not a.district and a.administrative_code == ^insee_code and
              a.administrative_code_type == "INSEE",
          select: a.id
        )
        |> Repo.one()
    end
  end

  def import_intensity(intensity, testimonies_mapping, events_mapping) do
    latitude = Map.get(intensity, "latitude")
    longitude = Map.get(intensity, "longitude")

    testimony_id = Map.get(intensity, "testimony_id")

    event_id = Map.get(intensity, "event_id")

    IO.inspect(events_mapping[event_id])

    code_insee = Map.get(intensity, "code_insee")

    is_nil(events_mapping[event_id]) && IO.inspect(event_id)

    time = events_mapping[event_id] && events_mapping[event_id].preferred_origin.time

    city_id =
      (code_insee && get_city_id(code_insee, time)) || get_city_id(longitude, latitude, time)

    cond do
      # There are missing event_id... TOFIX
      is_nil(event_id) ->
        nil

      is_nil(events_mapping[event_id]) ->
        IO.inspect("Missing event in mapping")
        IO.inspect(event_id)

        IO.inspect(
          "jq '.events[]|select(.id == #{event_id})' ~/Projects/import_testimonies/data/Testimonies.json"
        )

        nil

      true ->
        IO.inspect(intensity)

        {:ok, new_intensity} =
          %{
            event_id: event_id && events_mapping[event_id].id,
            testimony_id: Map.get(testimonies_mapping, testimony_id),
            testimony_preferred: Map.get(intensity, "testimony_preferred"),
            city_id: city_id,
            intensity: Map.get(intensity, "value") || 0,
            intensity_type: Map.get(intensity, "type"),
            method: Map.get(intensity, "method"),
            # method: Map.get(intensity, "source"),
            # evaluation_status
            quality: convert_quality(Map.get(intensity, "quality")),
            automatic: Map.get(intensity, "automatic"),
            author: Map.get(intensity, "author"),
            notes: Map.get(intensity, "notes"),
            city_preferred: Map.get(intensity, "city_preferred", false),
            city_administrative_code_type: code_insee && "INSEE",
            city_administrative_code: code_insee,
            longitude: longitude,
            latitude: latitude
          }
          |> Repo.upsert(Intensity)

        {intensity["id"], new_intensity.id}
    end
  end

  def import_intensities(intensities, testimonies_mapping, events_mapping) do
    intensities
    |> Enum.map(&import_intensity(&1, testimonies_mapping, events_mapping))
    |> Enum.filter(fn i -> not is_nil(i) end)
    |> Map.new()
  end

  def import_intensities_contributions(intensities_contributions, intensities_mapping) do
    intensities_contributions
    |> Enum.map(fn c ->
      pid = Map.get(c, "parent_intensity_id")
      id = Map.get(c, "intensity_id")

      %{
        parent_intensity_id: intensities_mapping[pid],
        intensity_id: intensities_mapping[id]
      }
      |> Repo.upsert(IntensityContribution)
    end)
  end

  def run(file) do
    Mix.Task.run("app.start")

    data = parse_file(file)
    events_mapping = get_events_mapping(data["events"])
    forms_mapping = import_forms(data["forms"])
    testimonies_mapping = import_testimonies(data["testimonies"], events_mapping, forms_mapping)

    intensities_mapping =
      import_intensities(data["intensities"], testimonies_mapping, events_mapping)

    import_intensities_contributions(data["intensities_contributions"], intensities_mapping)

    # data
    :ok
  end
end
