defmodule Mix.Tasks.Namazu.Hugo do
  use Mix.Task

  require Logger

  import Ecto.Query

  alias Web.Filters
  alias Namazu.Zone
  alias Namazu.Repo
  alias Namazu.Seismic

  @shortdoc "Generate Hugo files content"

  def prepare_phase(phase) do
    %{
      arrivalID: phase.id,
      originID: phase.origin_id,
      pickID: phase.pick.id,
      networkCode: phase.pick.stream.network_code,
      stationCode: phase.pick.stream.station_code,
      locationCode: phase.pick.stream.location_code,
      channelCode: phase.pick.stream.channel_code,
      azimuth: phase.azimuth && Float.round(phase.azimuth, 2),
      distance: phase.distance && Float.round(phase.distance, 2),
      phaseCode: phase.phase_code,
      time: phase.pick.time,
      timeResidual: phase.time_residual && Float.round(phase.time_residual, 2),
      automatic: phase.pick.automatic
    }
  end

  def event_title(locale, event_type, magnitude, city_name) do
    Gettext.with_locale(Web.Gettext, locale, fn ->
      event_type =
        event_type
        |> Filters.translate_event_type()
        |> String.capitalize()

      Gettext.gettext(
        Web.Gettext,
        "%{event_type} of magnitude %{magnitude}, near of %{city}",
        city: city_name,
        magnitude: magnitude,
        event_type: event_type
      )
    end)
  end

  def get_group(event) do
    cond do
      event.automatic -> 1
      event.eventtype == "earthquake" -> 1
      event.eventtype == "induced" -> 1
      true -> 2
    end
  end

  def stations_title(locale, zone) do
    Gettext.with_locale(Web.Gettext, locale, fn ->
      Gettext.gettext(Web.Gettext, "Stations of %{zone_name}", zone_name: zone.name)
    end)
  end

  def create_directory(base_directory, sub_directory) do
    directory = "#{base_directory}/#{sub_directory}"
    File.mkdir_p(directory)
    directory
  end

  def write_json_file(data, path) do
    content = Jason.encode!(data, pretty: true)
    :ok = File.write(path, content, [:write])
  end

  def write_markdown_files(directory, data, content \\ nil) do
    data.locales
    |> Enum.each(fn {code, i18n_data} ->
      front_matter =
        Map.merge(data.common, i18n_data)
        |> Jason.encode!(pretty: true)

      front_matter =
        if content do
          "#{front_matter}\n\n#{content}"
        else
          front_matter
        end

      file_path = "#{directory}/index.#{code}.md"
      :ok = File.write(file_path, front_matter, [:write])
    end)
  end

  def read_status_file(path) do
    status_file = File.open!(path, [:read, :write])

    last_sync_datetime =
      case status_file |> IO.read(:all) |> DateTime.from_iso8601() do
        {:ok, datetime, _} -> datetime
        {:error, :invalid_format} -> nil
      end

    {status_file, last_sync_datetime}
  end

  def export_networks(content_directory) do
    networks_directory = "#{content_directory}/networks"

    Namazu.Instruments.Network
    |> where([n], not n.hidden)
    |> Repo.all()
    |> Enum.each(&export_network(networks_directory, &1))
  end

  def export_network(networks_directory, network) do
    Logger.info("Exporting network: #{network.network_code}")
    directory = create_directory(networks_directory, network.network_code)

    write_markdown_files(directory, %{
      common: %{
        networkcode: network.network_code,
        color: network.color,
        description: network.description,
        headless: true
      },
      locales: %{
        en: %{},
        fr: %{}
      }
    })
  end

  def export_actors(content_directory) do
    actors_directory = "#{content_directory}/actors"

    Namazu.Actor
    |> Repo.all()
    |> Enum.each(&export_actor(actors_directory, &1))
  end

  def export_actor(actors_directory, actor) do
    Logger.info("Exporting actor : #{actor.name}")
    directory = create_directory(actors_directory, actor.slug)

    write_markdown_files(directory, %{
      common: %{
        title: actor.name,
        acronym: actor.acronym,
        slug: actor.acronym,
        countryiso2: actor.country_iso2,
        website: actor.url
      },
      locales: %{
        en: %{
          url: "/en/actors/#{actor.slug}"
        },
        fr: %{
          url: "/fr/acteurs/#{actor.slug}"
        }
      }
    })
  end

  def export_zones(hugo_directory) do
    Zone
    |> Repo.all()
    |> Enum.each(&export_zone(&1, hugo_directory))
  end

  def paginate_zone({events_chunk, index}, zone_api_directory) do
    json_path = "#{zone_api_directory}/page-#{index}.json"

    events_chunk
    |> Enum.map(fn event ->
      magnitude = Float.round(event.magnitude, 1)

      %{
        eventID: event.publicid,
        latitude: Float.round(event.latitude, 2),
        longitude: Float.round(event.longitude, 2),
        automatic: event.automatic,
        countryiso2: event.distinctivecity.countryiso2,
        magnitude: magnitude,
        localtime: event.localtime,
        group: get_group(event),
        description: %{
          en: event_title("en", event.eventtype, magnitude, event.distinctivecity.name),
          fr: event_title("fr", event.eventtype, magnitude, event.distinctivecity.name)
        }
      }
    end)
    |> write_json_file(json_path)
  end

  def paginate_events(hugo_directory) do
    Zone
    |> Repo.all()
    |> Enum.each(fn zone ->
      Logger.info("Paginate zone : #{zone.name}")
      json_directory = create_directory("#{hugo_directory}/static/json/zones", zone.slug)

      event_stream =
        event_query(zone)
        |> order_by([event: e], desc: e.localtime)
        |> Repo.stream()
        |> Stream.chunk_every(40)
        |> Stream.with_index(1)
        |> Task.async_stream(&paginate_zone(&1, json_directory),
          max_concurrency: 5
        )

      Repo.transaction(fn ->
        event_stream
        |> Stream.run()
      end)
    end)
  end

  def export_zone(zone, hugo_directory) do
    Logger.info("Exporting zone : #{zone.name}")
    directory = create_directory("#{hugo_directory}/content/zones", zone.slug)

    last_events = event_query(zone, nil, 40) |> Repo.all() |> Enum.map(&prepare_event(&1))

    %{
      description: zone.description,
      testimonyurl: zone.testimony_url,
      minimallongitude: zone.minimal_longitude,
      maximallongitude: zone.maximal_longitude,
      minimallatitude: zone.minimal_latitude,
      maximallatitude: zone.maximal_latitude,
      lastevents: last_events
    }
    |> write_json_file("#{hugo_directory}/data/zones/#{zone.slug}.json")

    # write_markdown_files(
    #   directory,
    #   %{
    #     common: %{
    #       slug: zone.slug,
    #       title: zone.name,
    #       weight: zone.weight
    #     },
    #     locales: %{
    #       en: %{
    #         url: "/en/zones/#{zone.slug}"
    #       },
    #       fr: %{
    #         url: "/fr/zones/#{zone.slug}"
    #       }
    #     }
    #   },
    #   zone.introduction
    # )

    Logger.info("Exporting stations zone : #{zone.name}")
    directory = create_directory("#{hugo_directory}/content/zones-stations", zone.slug)
    json_directory = create_directory("#{hugo_directory}/static/json/zones", zone.slug)

    stations =
      zone
      |> Web.Cache.get_stations_by_zone()

    stations_geojson =
      stations
      |> Enum.map(&Web.API.V1.EventController.to_feature(&1))
      |> Web.Helpers.geojson()

    write_json_file(stations_geojson, "#{json_directory}/stations.json")

    networks =
      stations
      |> Enum.map(&prepare_station/1)
      |> Enum.group_by(&Map.get(&1, :networkcode))
      |> Enum.map(fn {network_code, stations} ->
        %{
          networkcode: network_code,
          stations: stations,
          stations_count: length(stations)
        }
      end)
      |> Enum.sort(fn a, b -> a.stations_count >= b.stations_count end)

    write_markdown_files(
      directory,
      %{
        common: %{
          minimallongitude: zone.minimal_longitude,
          maximallongitude: zone.maximal_longitude,
          minimallatitude: zone.minimal_latitude,
          maximallatitude: zone.maximal_latitude,
          networks: networks,
          zone: %{
            name: zone.name,
            slug: zone.slug
          }
        },
        locales: %{
          en: %{
            title: stations_title("en", zone),
            url: "/en/zones/#{zone.slug}/stations"
          },
          fr: %{
            title: stations_title("fr", zone),
            url: "/fr/zones/#{zone.slug}/stations"
          }
        }
      }
    )
  end

  def prepare_station(station) do
    current_period = List.first(station.periods)

    %{
      networkcode: station.network.network_code,
      networkcolor: station.network.color,
      stationcode: station.station_code,
      latitude: Float.round(current_period.latitude, 2),
      longitude: Float.round(current_period.longitude, 2)
    }
  end

  def event_query(zone, last_sync \\ nil, last \\ nil) do
    query =
      Seismic.event_query()
      |> join(:inner, [event: e], po in assoc(e, :preferred_origin), as: :preferred_origin)
      |> join(:inner, [event: e], pm in assoc(e, :preferred_magnitude), as: :preferred_magnitude)
      |> join(:left, [preferred_origin: po], q in assoc(po, :quality), as: :quality)
      |> join(:left, [event: e], c in assoc(e, :distinctive_city), as: :distinctive_city)
      |> where([event: e], e.zone_id == ^zone.id)
      |> select(
        [
          event: e,
          preferred_origin: po,
          preferred_magnitude: pm,
          distinctive_city: c,
          quality: q
        ],
        %{
          publicid: e.publicid,
          date: po.time,
          localtime: e.localtime,
          standarderror: q.standard_error,
          usedstationcount: q.used_station_count,
          usedphasecount: q.used_phase_count,
          azimuthalgap: q.azimuthal_gap,
          eventtype: e.event_type,
          automatic: po.automatic,
          evaluationstatus: po.evaluation_status,
          latitude: po.latitude,
          latitudeuncertainty: po.latitude_uncertainty,
          longitude: po.longitude,
          longitudeuncertainty: po.longitude_uncertainty,
          depth: po.depth,
          depthtype: po.depth_type,
          magnitude: pm.magnitude,
          magnitudetype: pm.magnitude_type,
          zone: %{
            name: ^zone.name,
            slug: ^zone.slug
          },
          timezone: e.timezone,
          distinctivecity: %{
            name: c.name,
            countryiso2: c.country_iso2,
            distance: e.distinctive_city_distance
          }
        }
      )

    query =
      if last_sync do
        query
        |> where(
          [event: e, preferred_origin: po, preferred_magnitude: pm],
          e.inserted_at > ^last_sync or
            e.updated_at > ^last_sync or
            po.inserted_at > ^last_sync or
            po.updated_at > ^last_sync or
            pm.inserted_at > ^last_sync or
            pm.updated_at > ^last_sync
        )
      else
        query
      end

    if last do
      query |> limit(^last) |> order_by([preferred_origin: po], desc: po.time)
    else
      query
    end
  end

  def export_events(hugo_directory, last_sync \\ nil, last \\ nil) do
    Zone
    |> Repo.all()
    |> Enum.map(fn zone ->
      event_stream =
        event_query(zone, last_sync, last)
        |> Repo.stream()
        |> Task.async_stream(&export_event(&1, hugo_directory),
          max_concurrency: 5
        )

      {:ok, count} =
        Repo.transaction(fn ->
          event_stream
          |> Enum.count()

          # |> Stream.run()
        end)

      count
    end)
    |> Enum.sum()
  end

  def prepare_event(event) do
    event =
      event
      # |> Map.put(:eventtype, (event.eventtype || "event") |> String.replace(" ", "_"))
      |> Map.put(:eventtype, event.eventtype || "event")
      |> Map.put(:magnitude, Float.round(event.magnitude, 1))
      |> Map.put(:depth, round(event.depth))
      |> Map.put(:longitude, Float.round(event.longitude, 2))
      |> Map.put(:latitude, Float.round(event.latitude, 2))
      |> Map.put(:azimuthalgap, Float.round(event.azimuthalgap, 2))
      |> Map.put(:standarderror, event.standarderror && Float.round(event.standarderror, 2))
      |> Map.put(
        :longitudeuncertainty,
        Web.Filters.rounded_uncertainty(event.longitudeuncertainty)
      )
      |> Map.put(
        :latitudeuncertainty,
        Web.Filters.rounded_uncertainty(event.latitudeuncertainty)
      )
      |> Map.put(:localtime, Namazu.local_datetime(event.date, event.timezone))
      |> Map.put(:group, get_group(event))
      |> Map.delete(:timezone)
      |> Map.delete(:location)

    if event.automatic or event.eventtype == "earthquake" do
      Map.put(event, :testimonyurl, "http://www.franceseisme.fr/formulaire/index.php?IdSei=0")
    else
      event
    end
  end

  def export_event(event, hugo_directory) do
    Logger.info("Exporting event : #{event.publicid}")
    directory = create_directory("#{hugo_directory}/content/events", event.publicid)

    # json_directory = create_directory("#{hugo_directory}/static/json/events", event.id)

    phases =
      Web.Cache.get_preferred_origin_phases(event.publicid)
      |> Enum.map(&prepare_phase(&1))
      |> Enum.map(&Map.delete(&1, :arrivalID))
      |> Enum.map(&Map.delete(&1, :channelCode))
      |> Enum.map(&Map.delete(&1, :locationCode))
      |> Enum.map(&Map.delete(&1, :originID))
      |> Enum.map(&Map.delete(&1, :pickID))

    past_seismicity =
      Web.Cache.get_past_seismicity(event.publicid)
      |> Enum.map(fn event ->
        %{
          publicid: event.publicid,
          time: event.preferred_origin.time,
          magnitude: Float.round(event.preferred_magnitude.magnitude, 1),
          latitude: Float.round(event.preferred_origin.latitude, 2),
          longitude: Float.round(event.preferred_origin.longitude, 2)
        }
      end)

    cities =
      Web.Cache.get_distintive_cities(event.publicid)
      |> Enum.map(&Web.API.V1.EventController.get_properties(&1))
      |> Enum.map(&Map.delete(&1, :areaID))

    event =
      prepare_event(event)
      |> Map.put(:cities, cities)
      |> Map.put(:pastseismicity, past_seismicity)
      |> Map.put(:phases, phases)

    write_json_file(event, "#{hugo_directory}/data/events/#{event.publicid}.json")

    write_markdown_files(
      directory,
      %{
        common: %{
          publicid: event.publicid,
          zones: [event.zone.slug]
        },
        locales: %{
          en: %{
            url: "/en/events/#{event.publicid}",
            title: event_title("en", event.eventtype, event.magnitude, event.distinctivecity.name)
          },
          fr: %{
            url: "/fr/evenements/#{event.publicid}",
            title: event_title("fr", event.eventtype, event.magnitude, event.distinctivecity.name)
          }
        }
      }
    )

    :ok
  end

  def run(args) do
    Mix.Task.run("app.start")

    {options, args, _} = OptionParser.parse(args, switches: [last: :integer])

    now = DateTime.utc_now()

    hugo_directory = List.first(args)

    data_directory = create_directory(hugo_directory, "data")
    create_directory(data_directory, "events")
    create_directory(data_directory, "zones")

    content_directory = "#{hugo_directory}/content"

    {status_file, last_sync} = read_status_file("#{hugo_directory}/.sync")

    # export_actors(content_directory)
    export_zones(hugo_directory)
    export_networks(content_directory)
    events_updated_count = export_events(hugo_directory, last_sync, options[:last])

    if events_updated_count > 0 do
      paginate_events(hugo_directory)
      File.touch("#{hugo_directory}/.updated")
    end

    :file.position(status_file, 0)
    IO.write(status_file, now |> DateTime.to_iso8601())
  end
end
