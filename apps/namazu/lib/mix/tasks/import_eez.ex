defmodule Mix.Tasks.Namazu.ImportEez do
  use Mix.Task

  alias Namazu.Geography
  alias Namazu.Repo

  @shortdoc "Import Exclusive Economic Zone from a JSON file"

  def update_params(params, geometry = %Geo.Point{}) do
    params |> Map.put(:location, geometry)
  end

  def update_params(params, geometry) do
    params |> Map.put(:boundary, Geography.polygon_to_multi(geometry))
  end

  def upsert(feature) do
    properties = feature["properties"]

    {:ok, area} =
      %{
        area_type: "eez",
        country_iso3: Map.get(properties, "country_iso3"),
        source: Map.get(properties, "source"),
        source_version: Map.get(properties, "source_version"),
        source_id: Map.get(properties, "source_id")
      }
      |> Repo.upsert(Geography.Area)

    {:ok, _area_eriod} =
      %{
        area_id: area.id,
        longitude: Map.get(properties, "longitude"),
        latitude: Map.get(properties, "latitude"),
        name: Map.get(properties, "name"),
        source: Map.get(properties, "source"),
        source_version: Map.get(properties, "source_version"),
        source_id: Map.get(properties, "source_id"),
        boundary:
          Geo.JSON.decode!(feature["geometry"])
          |> Map.put(:srid, 4326)
          |> Geography.polygon_to_multi()
      }
      |> Repo.upsert(Geography.AreaPeriod)
  end

  def run(args) do
    Mix.Task.run("app.start")

    {_options, file, _} = OptionParser.parse(args, strict: [])

    File.read!(file)
    |> Jason.decode!()
    |> Map.get("features")
    |> Enum.each(&upsert(&1))
  end
end
