defmodule Mix.Tasks.Namazu.ImportCities do
  use Mix.Task
  use Timex

  alias Namazu.Geography
  alias Namazu.Repo

  @shortdoc "Import cities data from a JSON file"

  def update_params(params, geometry = %Geo.Point{}) do
    params |> Map.put(:location, geometry)
  end

  def update_params(params, geometry) do
    params |> Map.put(:boundary, Geography.polygon_to_multi(geometry))
  end

  def upsert(feature) do
    properties = feature["properties"]
    geometry = Geo.JSON.decode!(feature["geometry"]) |> Map.put(:srid, 4326)

    area_params = %{
      area_type: "city",
      country_iso2: Map.get(properties, "country_iso2"),
      district: Map.get(properties, "district", false),
      administrative_code_type: Map.get(properties, "administrative_code_type"),
      administrative_code: Map.get(properties, "administrative_code"),
      source: Map.get(properties, "source"),
      source_version: Map.get(properties, "source_version"),
      source_id: Map.get(properties, "source_id"),
      geoname_id: Map.get(properties, "geoname_id")
    }

    {:ok, area} = Repo.upsert(area_params, Geography.Area)

    {:ok, _} =
      %{
        area_id: area.id,
        name: Map.get(properties, "name"),
        name_ar: Map.get(properties, "name_ar"),
        name_bn: Map.get(properties, "name_bn"),
        name_de: Map.get(properties, "name_de"),
        name_el: Map.get(properties, "name_el"),
        name_en: Map.get(properties, "name_en"),
        name_es: Map.get(properties, "name_es"),
        name_fr: Map.get(properties, "name_fr"),
        name_hi: Map.get(properties, "name_hi"),
        name_hu: Map.get(properties, "name_hu"),
        name_id: Map.get(properties, "name_id"),
        name_it: Map.get(properties, "name_it"),
        name_ja: Map.get(properties, "name_ja"),
        name_ko: Map.get(properties, "name_ko"),
        name_nl: Map.get(properties, "name_nl"),
        name_pl: Map.get(properties, "name_pl"),
        name_pt: Map.get(properties, "name_pt"),
        name_ru: Map.get(properties, "name_ru"),
        name_sv: Map.get(properties, "name_sv"),
        name_tr: Map.get(properties, "name_tr"),
        name_vi: Map.get(properties, "name_vi"),
        name_zh: Map.get(properties, "name_zh"),
        period_end: Map.get(properties, "period_end"),
        period_start: Map.get(properties, "period_start"),
        population:
          Map.get(properties, "population") && Kernel.trunc(Map.get(properties, "population")),
        source: Map.get(properties, "source"),
        source_id: Map.get(properties, "source_id"),
        source_version: Map.get(properties, "source_version")
      }
      |> update_params(geometry)
      |> Repo.upsert(Geography.AreaPeriod)
  end

  def run(args) do
    Mix.Task.run("app.start")

    {options, file, _} = OptionParser.parse(args, strict: [exclude_country_iso2: :string])

    excluded_country_iso2 = Keyword.get(options, :exclude_country_iso2, "") |> String.split(",")

    File.read!(file)
    |> Jason.decode!()
    |> Map.get("features")
    |> Enum.filter(fn f -> f["properties"]["country_iso2"] not in excluded_country_iso2 end)
    |> Enum.each(&upsert(&1))
  end
end
