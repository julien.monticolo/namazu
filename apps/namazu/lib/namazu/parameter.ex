defmodule Namazu.Parameter do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :namazu

  @primary_key {:key, :string, autogenarate: false}

  schema "parameters" do
    field(:value, :string)
  end

  def distinctive_fields() do
    [:key]
  end

  def changeset(parameter, params \\ %{}) do
    parameter
    |> cast(params, [:key, :value])
  end
end
