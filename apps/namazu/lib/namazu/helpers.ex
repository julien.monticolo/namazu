defmodule Namazu.Helpers do
  def parse_float(_value = ""), do: nil
  def parse_float(_value = nil), do: nil

  def parse_float(value) do
    {float, _} = Float.parse(value)
    float
  end

  def parse_integer(_value = ""), do: nil
  def parse_integer(_value = nil), do: nil
  def parse_integer(value), do: String.to_integer(value)

  def parse_datetime(_datetime = ""), do: nil
  def parse_datetime(_datetime = nil), do: nil

  def parse_datetime(datetime) do
    case DateTime.from_iso8601(datetime) do
      {:ok, datetime, _offset} ->
        datetime

      {:error, :missing_offset} ->
        {:ok, datetime, _offset} = DateTime.from_iso8601(datetime <> "Z")
        datetime
    end
  end

  def parse_order_by(_value = ""), do: nil
  def parse_order_by(_value = nil), do: nil

  def parse_order_by(value) do
    case value do
      "time" -> :descending_time
      "time-asc" -> :ascending_time
      "magnitude" -> :descending_magnitude
      "magnitude-asc" -> :ascending_magnitude
      "ascending_time" -> :ascending_time
      "descending_time" -> :descending_time
      "ascending_magnitude" -> :ascending_magnitude
      "descending_magnitude" -> :descending_magnitude
      _ -> nil
    end
  end

  @spec _nullify(map(), []) :: map()
  defp _nullify(map, []), do: map

  defp _nullify(map, [key | remaining_keys]) do
    case Map.get(map, key) do
      "" -> Map.put(map, key, nil)
      _ -> _nullify(map, remaining_keys)
    end
  end

  @spec nullify(map()) :: map()
  def nullify(map), do: _nullify(map, Map.keys(map))
end
