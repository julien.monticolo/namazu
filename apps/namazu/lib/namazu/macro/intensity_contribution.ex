defmodule Namazu.Macro.IntensityContribution do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :macro

  schema "intensities_contributions" do
    belongs_to(:intensity, Namazu.Macro.Intensity)
    belongs_to(:parent_intensity, Namazu.Macro.Intensity)

    field(:weight, :float)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [
      :parent_intensity_id,
      :intensity_id
    ]
  end

  def changeset(intensity, params \\ {}) do
    intensity
    |> cast(params, [
      :parent_intensity_id,
      :intensity_id
    ])
  end
end
