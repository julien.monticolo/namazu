defmodule Namazu.Macro.Form do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Macro.{Form, Question}

  @schema_prefix :macro

  schema "forms" do
    field(:name, :string)
    field(:type, :string)
    field(:default, :boolean)

    has_many(:questions, Question)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:name]
  end

  def changeset(form, params \\ %{}) do
    form
    |> cast(params, [:name, :type, :default])

    # |> cast_assoc(:questions)
    # |> validate_required([:name])
  end

  @doc """
    Returns the root question of a form
  """
  def root_question(form) do
    %Form{id: form_id} = form
    Question |> preload(:question_choices) |> Namazu.Repo.get_by(%{form_id: form_id, order: 1})
  end

  def questions(form) do
    Question
    |> preload(:question_choices)
    |> where(form_id: ^form.id)
    |> order_by(:order)
    |> Repo.all()
  end
end
