defmodule Namazu.Macro.CityIntensity do
  use Ecto.Schema

  @schema_prefix :macro

  schema "city_intensities" do
    field(:method, :string)
    field(:evaluation_status, :string)
    field(:automatic, :boolean)
    field(:quality, :string)
    field(:author, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end
end
