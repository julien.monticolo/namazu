defmodule Namazu.Macro.QuestionCondition do
  use Ecto.Schema
  import Ecto.Changeset
  alias Namazu.Macro.{Question, QuestionChoice}
  @schema_prefix :macro

  schema "question_conditions" do
    belongs_to(:question, Question)
    belongs_to(:question_choice, QuestionChoice)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(question_condition, params \\ %{}) do
    question_condition
    |> cast(params, [:question_choice_id])
  end
end
