defmodule Namazu.Zone do
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  alias Namazu.Seismic
  alias Namazu.Repo

  @schema_prefix :namazu

  schema "zones" do
    field(:minimal_latitude, :float)
    field(:maximal_latitude, :float)
    field(:minimal_longitude, :float)
    field(:maximal_longitude, :float)

    field(:name, :string)
    field(:slug, :string)
    field(:weight, :integer)
    field(:buffer, :integer)
    field(:description, :string)
    field(:introduction, :string)

    field(:testimony_url, :string)

    field(:show_manual, :boolean)
    field(:manual_minimal_magnitude, :float)
    field(:manual_maximal_depth, :float)
    field(:manual_minimal_used_phase_count, :integer)
    field(:manual_event_types, {:array, :string})

    field(:show_automatic, :boolean)
    field(:automatic_minimal_magnitude, :float)
    field(:automatic_maximal_depth, :float)
    field(:automatic_minimal_used_phase_count, :integer)

    field(:boundary, Geo.PostGIS.Geometry)
  end

  def distinctive_fields() do
    [:slug]
  end

  def changeset(zone, params \\ %{}) do
    zone
    |> cast(params, [
      :minimal_latitude,
      :maximal_latitude,
      :minimal_longitude,
      :maximal_longitude,
      :name,
      :slug,
      :weight,
      :buffer,
      :description,
      :introduction,
      :show_manual,
      :manual_minimal_magnitude,
      :manual_maximal_depth,
      :manual_minimal_used_phase_count,
      :manual_event_types,
      :show_automatic,
      :automatic_minimal_magnitude,
      :automatic_maximal_depth,
      :automatic_minimal_used_phase_count
    ])
  end

  def get_events(zone) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> Seismic.preload_preferred_origin_quality()
    |> Seismic.belongs_to(zone)
  end

  def get_stations(zone) do
    from(s in Namazu.Instruments.Station,
      join: sp in assoc(s, :periods),
      join: n in assoc(s, :network),
      where: is_nil(sp.period_end) and not s.hidden and not n.hidden,
      where: sp.longitude >= ^zone.minimal_longitude and sp.longitude <= ^zone.maximal_longitude,
      where: sp.latitude >= ^zone.minimal_latitude and sp.latitude <= ^zone.maximal_latitude,
      preload: [periods: sp, network: n],
      order_by: [asc: n.network_code, asc: s.station_code]
    )
  end

  def get_from_location(_location = %Geo.Point{coordinates: {longitude, latitude}}) do
    __MODULE__
    |> where(
      [z],
      z.minimal_longitude <= ^longitude and z.maximal_longitude >= ^longitude and
        z.minimal_latitude <= ^latitude and z.maximal_latitude >= ^latitude
    )
    |> order_by(desc: :weight)
    |> limit(1)
    |> Repo.one()
  end
end
