defmodule Namazu.Seismic.StationMagnitudeContribution do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "stationmagnitudes_contributions" do
    belongs_to(:magnitude, Namazu.Seismic.Magnitude)
    belongs_to(:station_magnitude, Namazu.Seismic.StationMagnitude)

    field(:residual, :float)
    field(:weight, :float)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:magnitude_id, :station_magnitude_id, :residual, :weight]
  end

  def changeset(station_magnitude_contribution, params \\ %{}) do
    station_magnitude_contribution
    |> cast(params, [:magnitude_id, :station_magnitude_id, :residual, :weight])
  end
end
