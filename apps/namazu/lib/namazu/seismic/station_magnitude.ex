defmodule Namazu.Seismic.StationMagnitude do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "stationmagnitudes" do
    belongs_to(:amplitude, Namazu.Seismic.Amplitude)
    belongs_to(:stream, Namazu.Seismic.Stream)
    has_many(:station_magnitude_contributions, Namazu.Seismic.StationMagnitudeContribution)

    field(:magnitude, :float)
    field(:magnitude_type, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:amplitude_id, :stream_id, :magnitude, :magnitude_type]
  end

  def changeset(station_magnitude, params \\ %{}) do
    station_magnitude
    |> cast(params, [:amplitude_id, :stream_id, :magnitude, :magnitude_type, :created_at])
  end
end
