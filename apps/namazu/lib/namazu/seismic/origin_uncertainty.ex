defmodule Namazu.Seismic.OriginUncertainty do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic
  @primary_key {:origin_id, :id, autogenerate: false}

  schema "origins_uncertainties" do
    field(:horizontal, :float)
    field(:minimum_horizontal, :float)
    field(:maximum_horizontal, :float)
    field(:confidenceellipsoid_semimajoraxislength, :float)
    field(:confidenceellipsoid_semiminoraxislength, :float)
    field(:confidenceellipsoid_semiintermediateaxislength, :float)
    field(:confidenceellipsoid_majoraxisplunge, :float)
    field(:confidenceellipsoid_majoraxisazimuth, :float)
    field(:confidenceellipsoid_majoraxisrotation, :float)
    field(:confidenceellipsoid_used, :boolean)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields(), do: [:origin_id]

  def changeset(origin_uncertainty, params \\ %{}) do
    origin_uncertainty
    |> cast(params, [
      :origin_id,
      :horizontal,
      :minimum_horizontal,
      :maximum_horizontal,
      :confidenceellipsoid_semimajoraxislength,
      :confidenceellipsoid_semiminoraxislength,
      :confidenceellipsoid_semiintermediateaxislength,
      :confidenceellipsoid_majoraxisplunge,
      :confidenceellipsoid_majoraxisazimuth,
      :confidenceellipsoid_majoraxisrotation,
      :confidenceellipsoid_used
    ])
  end
end
