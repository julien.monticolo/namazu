defmodule Namazu.Seismic.OriginQuality do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic
  @primary_key {:origin_id, :id, autogenerate: false}

  schema "origins_qualities" do
    field(:associated_phase_count, :integer)
    field(:used_phase_count, :integer)
    field(:associated_station_count, :integer)
    field(:used_station_count, :integer)
    field(:standard_error, :float)
    field(:azimuthal_gap, :float)
    field(:maximum_distance, :float)
    field(:minimum_distance, :float)
    field(:median_distance, :float)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields(), do: [:origin_id]

  def changeset(origin_quality, params \\ %{}) do
    origin_quality
    |> cast(params, [
      :origin_id,
      :associated_phase_count,
      :used_phase_count,
      :associated_station_count,
      :used_station_count,
      :standard_error,
      :azimuthal_gap,
      :maximum_distance,
      :minimum_distance,
      :median_distance
    ])
  end
end
