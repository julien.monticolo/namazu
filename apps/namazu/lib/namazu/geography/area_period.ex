defmodule Namazu.Geography.AreaPeriod do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :geography

  schema "areas_periods" do
    belongs_to(:area, Namazu.Geography.Area)

    field(:population, :integer)

    field(:name, :string)
    field(:name_ar, :string)
    field(:name_bn, :string)
    field(:name_de, :string)
    field(:name_el, :string)
    field(:name_en, :string)
    field(:name_es, :string)
    field(:name_fr, :string)
    field(:name_hi, :string)
    field(:name_hu, :string)
    field(:name_id, :string)
    field(:name_it, :string)
    field(:name_ja, :string)
    field(:name_ko, :string)
    field(:name_nl, :string)
    field(:name_pl, :string)
    field(:name_pt, :string)
    field(:name_ru, :string)
    field(:name_sv, :string)
    field(:name_tr, :string)
    field(:name_vi, :string)
    field(:name_zh, :string)

    field(:latitude, :float)
    field(:longitude, :float)
    field(:location, Geo.PostGIS.Geometry)
    field(:location_type, :string)
    field(:boundary, Geo.PostGIS.Geometry)

    field(:period_start, :utc_datetime_usec)
    field(:period_end, :utc_datetime_usec)

    field(:source, :string)
    field(:source_version, :string)
    field(:source_id, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:area_id, :period_start]
  end

  def changeset(country, params \\ %{}) do
    country
    |> cast(params, [
      :area_id,
      :population,
      :name,
      :name_ar,
      :name_bn,
      :name_de,
      :name_el,
      :name_en,
      :name_es,
      :name_fr,
      :name_hi,
      :name_hu,
      :name_id,
      :name_it,
      :name_ja,
      :name_ko,
      :name_nl,
      :name_pl,
      :name_pt,
      :name_ru,
      :name_sv,
      :name_tr,
      :name_vi,
      :name_zh,
      :longitude,
      :latitude,
      :period_start,
      :period_end,
      :source,
      :source_version,
      :source_id,
      :boundary,
      :location
    ])
  end
end
