defmodule Namazu.Geography.Area do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :geography

  schema "areas" do
    has_many(:periods, Namazu.Geography.AreaPeriod)

    field(:area_type, :string)
    field(:district, :boolean)

    field(:geoname_id, :integer)
    field(:geoname_name, :string)

    field(:country_iso2, :string)
    field(:country_iso3, :string)

    field(:administrative_code_type, :string)
    field(:administrative_code, :string)

    field(:source, :string)
    field(:source_version, :string)
    field(:source_id, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    # [:area_type, :administrative_code_type, :administrative_code]
    [:area_type, :source, :source_id]
  end

  def changeset(country, params \\ %{}) do
    country
    |> cast(params, [
      :area_type,
      :district,
      :geoname_id,
      :geoname_name,
      :country_iso2,
      :country_iso3,
      :administrative_code_type,
      :administrative_code,
      :source,
      :source_version,
      :source_id
    ])
  end
end
