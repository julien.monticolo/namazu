defmodule Namazu.Geography do
  import Ecto.Query

  alias Namazu.Repo

  def polygon_to_multi(geom = %Geo.MultiPolygon{}), do: geom

  def polygon_to_multi(geom = %Geo.Polygon{}) do
    %Geo.MultiPolygon{
      coordinates: [geom.coordinates],
      srid: geom.srid,
      properties: geom.properties
    }
  end

  defmacro geography_st_within(geographyA, geographyB) do
    quote do:
            fragment(
              "ST_Within(?::geometry,?::geometry)",
              unquote(geographyA),
              unquote(geographyB)
            )
  end

  def distintive_cities(location, limit \\ 15) do
    from(
      a in Namazu.Geography.City,
      join: h in fragment("geography.get_nearest_cities(?, ?)", ^location, ^limit),
      where: a.id == h.id,
      select: {
        a,
        fragment(
          "(ST_Distance(?::geography, ?::geography)/1000)::integer",
          ^location,
          a.location
        )
      }
    )
    |> order_by(desc: :population)
    |> limit(^limit)
    |> Repo.all()
    |> Enum.map(fn {city, distance} ->
      Map.put(city, :distance, distance)
    end)
  end
end
