defmodule Seiscomp3.Syncer do
  @callback query() :: Ecto.Query.t()
  @callback import_entry(entry :: term) ::
              {:ok, term, term, integer} | {:missing_match, term} | {:error, %Ecto.Changeset{}}
  @callback state_key() :: String.t()
end
