defmodule Seiscomp3.Data.Events do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query

  alias Namazu.Seismic.Event

  def state_key, do: "event"

  def query do
    from(
      e in "event",
      join: pe in "publicobject",
      on: [_oid: e._oid],
      order_by: [e._last_modified, e._oid],
      select: %{
        mapping: %{
          event_oid: e._oid,
          event_publicid: pe.m_publicid
        },
        event: %{
          event_type: e.m_type
        },
        last_modified: e._last_modified,
        oid: e._oid
      }
    )
  end

  def prepare(event_map) do
    %{event_map | event_type: Event.seiscomp3_to_namazu_event_type(event_map.event_type)}
  end

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")
    mapping_map = entry.mapping

    event_map =
      entry.event
      |> prepare()

    Logger.info("Importing : #{Namazu.to_string(event_map)}")

    [_mapping, event] = Seiscomp3.Mapping.upsert!(mapping_map, event_map)

    {:ok, event, entry.last_modified, entry.oid}
  end
end
