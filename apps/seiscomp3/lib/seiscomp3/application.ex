defmodule Seiscomp3.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: true

    children =
      case [System.get_env("SC3_DATABASE"), Application.get_env(:namazu, :environment)] do
        [nil, _] -> []
        ["", _] -> []
        [_, :prod] -> [supervisor(Seiscomp3.Repo, []), worker(Seiscomp3.Process, [])]
        [_, _] -> [supervisor(Seiscomp3.Repo, [])]
      end

    opts = [strategy: :one_for_one, name: Seiscomp3.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
