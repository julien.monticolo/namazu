defmodule Seiscomp3.Repo do
  use Ecto.Repo, otp_app: :seiscomp3, adapter: Ecto.Adapters.Postgres

  def init(_type, config) do
    config =
      config
      |> Keyword.put(
        :url,
        System.get_env("SC3_DATABASE", "postgres://postgres@127.0.0.1:5433/postgres")
      )

    {:ok, config}
  end
end
